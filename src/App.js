import "./asset/css/general.scss"
import { useState } from 'react'
import LandingPageRoute from "./routing/LandingPageRoute";
function App() {
  const [qrcodeModal, setQrcodeModal] = useState(false);

  const toggleQrcodeModal = () => setQrcodeModal(!qrcodeModal);

  const closeQrcodeBtn = <button className="btn-close" onClick={toggleQrcodeModal}></button>;

  return (
    <div className="base">
        <LandingPageRoute qrcodeModal = {qrcodeModal} toggleQrcodeModal={toggleQrcodeModal}  closeQrcodeBtn={closeQrcodeBtn} />
    </div>
  );
}

export default App;
