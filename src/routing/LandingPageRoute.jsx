import Promo from "../component/landing-page/page/Promo";
import LetsWin from "../component/landing-page/component/learnmore/learn-win"
import LearnQR from "../component/landing-page/component/learnmore/learn-qr";
import LearnRecurring from "../component/landing-page/component/learnmore/learn-recurring";
import LearnMutation from "../component/landing-page/component/learnmore/learn-mutation";
import Learn from "../component/landing-page/page/Learn";
import Privacy from "../component/landing-page/page/Privacy";
import TermsandCondition from "../component/landing-page/page/TermsAndCondition"
import Home from "../component/landing-page/page/Home";
import Feature from "../component/landing-page/page/Feature";
import { Route,Switch} from "react-router-dom";
import ScrollToTop from "./ScrollToTop"
import FaqAndHelp from "../component/landing-page/page/FaqAndHelp";

const LandingPageRoute = ({qrcodeModal,toggleQrcodeModal,closeQrcodeBtn}) => {
    return (
        <>
        <Switch>
            <Route exact path="/privacy">
            <ScrollToTop/>
                <Privacy 
                    isOpenModal={qrcodeModal} 
                    toggleModal={toggleQrcodeModal}
                    closeBtnModal={closeQrcodeBtn}
                />
            </Route>
            <Route exact path="/terms-and-condition">
                <ScrollToTop/>
                <TermsandCondition
                    isOpenModal={qrcodeModal} 
                    toggleModal={toggleQrcodeModal}
                    closeBtnModal={closeQrcodeBtn}
                />
            </Route>
            <Route exact path="/feature">
                <ScrollToTop/>
                <Feature 
                isOpenModal={qrcodeModal} 
                toggleModal={toggleQrcodeModal}
                closeBtnModal={closeQrcodeBtn}
                />
            </Route>
            <Route exact path="/letswin">
                <ScrollToTop/>
                <LetsWin isOpenModal={qrcodeModal} 
                toggleModal={toggleQrcodeModal}
                closeBtnModal={closeQrcodeBtn} />
            </Route>
            <Route exact path="/scanqrcode">
                <ScrollToTop/>
                <LearnQR isOpenModal={qrcodeModal} 
                toggleModal={toggleQrcodeModal}
                closeBtnModal={closeQrcodeBtn} />
            </Route>
            <Route exact path="/recurring">
                <ScrollToTop/>
                <LearnRecurring isOpenModal={qrcodeModal} 
                toggleModal={toggleQrcodeModal}
                closeBtnModal={closeQrcodeBtn} />
            </Route>
            <Route exact path="/mutation">
                <ScrollToTop/>
                <LearnMutation isOpenModal={qrcodeModal} 
                toggleModal={toggleQrcodeModal}
                closeBtnModal={closeQrcodeBtn} />
            </Route>
            <Route exact path="/">
                <ScrollToTop/>
                <Home
                    isOpenModal={qrcodeModal} 
                    toggleModal={toggleQrcodeModal}
                    closeBtnModal={closeQrcodeBtn} 
                />
            </Route>
            <Route path="/promo">
                <Promo
                    isOpenModal={qrcodeModal} 
                    toggleModal={toggleQrcodeModal}
                    closeBtnModal={closeQrcodeBtn} 
                />
            </Route>
            <Route exact path="/jaga-aja">
                <ScrollToTop/>
                <Learn
                    isOpenModal={qrcodeModal} 
                    toggleModal={toggleQrcodeModal}
                    closeBtnModal={closeQrcodeBtn} 
                />
            </Route>
            <Route exact path="/beraksi">
                <ScrollToTop/>
                <Learn
                    isOpenModal={qrcodeModal} 
                    toggleModal={toggleQrcodeModal}
                    closeBtnModal={closeQrcodeBtn} 
                />
            </Route>
            <Route exact path="/help">
                <ScrollToTop/>
                <FaqAndHelp 
                    isOpenModal={qrcodeModal} 
                    toggleModal={toggleQrcodeModal}
                    closeBtnModal={closeQrcodeBtn} 
                />
            </Route>
            <Route path="/faq">
                <ScrollToTop/>
                <FaqAndHelp 
                    isOpenModal={qrcodeModal} 
                    toggleModal={toggleQrcodeModal}
                    closeBtnModal={closeQrcodeBtn} 
                />
            </Route>
        </Switch>
        </>
    )
}

export default LandingPageRoute
