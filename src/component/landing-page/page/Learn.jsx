import LearnMore from "../component/learnmore/learn"
import Footer from "../component/footer/Footer"
import TopNavbar from "../component/header/TopNavbar"

const Learn = ({isOpenModal, toggleModal, closeBtnModal}) => {
    return (
        <div>
            <TopNavbar
                isOpenModal={isOpenModal} 
                toggleModal={toggleModal}
                closeBtnModal={closeBtnModal} 
            />
            <LearnMore
                toggleModal={toggleModal}
            />
            <Footer />
        </div>
    )
}

export default Learn
