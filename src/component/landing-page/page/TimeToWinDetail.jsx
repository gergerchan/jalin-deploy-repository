import Content from "../component/content-carousel/Content"
import PromoCarousel from "../component/promo-carousel/PromoCarousel"
import Footer from "../component/footer/Footer"
import TopNavbar from "../component/header/TopNavbar"
const TimeToWinDetail = () => {
    return (
        <div>
             <TopNavbar />
             <PromoCarousel />
             {/* <Content/> */}
             <Footer />
        </div>
    )
}

export default TimeToWinDetail
