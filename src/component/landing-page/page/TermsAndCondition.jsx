import Footer from "../component/footer/Footer"
import TopNavbar from "../component/header/TopNavbar"
import TermsContent from "../component/terms-content/TermsContent"
const TermsAndCondition = ({isOpenModal, toggleModal, closeBtnModal}) => {
    return (
        <>
            <TopNavbar 
                isOpenModal={isOpenModal} 
                toggleModal={toggleModal}
                closeBtnModal={closeBtnModal} 
            />
            <TermsContent website="bankjalin.xyz"/>
            <Footer />
        </>
    )
}

export default TermsAndCondition
