import Footer from "../component/footer/Footer"
import TopNavbar from "../component/header/TopNavbar"
import PrivacyContent from "../component/privacy-content/PrivacyContent"
const Privacy = ({isOpenModal, toggleModal, closeBtnModal}) => {
    return (
        <>
            <TopNavbar 
                isOpenModal={isOpenModal} 
                toggleModal={toggleModal}
                closeBtnModal={closeBtnModal} 
            />
            <PrivacyContent website="bankjalin.xyz"/>
            <Footer />
        </>
    )
}

export default Privacy
