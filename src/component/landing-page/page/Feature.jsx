import { Fragment } from "react"
import Footer from "../component/footer/Footer"
import TopNavbar from "../component/header/TopNavbar"
import ContentFeatureTop from "../component/feature/feature-top"
import Contentfeature from "../component/feature/feature-content"
import Contentjalin from "../component/content/Contentjalin"
import image1 from "../../../asset/img/feature-image1.png"
import image2 from "../../../asset/img/feature-image2.png"
import image3 from "../../../asset/img/feature-image3.png"
import image4 from "../../../asset/img/feature-image4.png"
import image5 from "../../../asset/img/feature-image5.png"
import image6 from "../../../asset/img/feature-image6.png"
import image7 from "../../../asset/img/feature-image7.png"
import candle from "../../../asset/img/home-candle.svg"
import jaga from "../../../asset/img/feature-jaga.svg"
import pig from "../../../asset/img/feature-beraksi.svg"

const Feature = ({ isOpenModal, toggleModal, closeBtnModal }) => {
    return (
        <>
            <TopNavbar
                isOpenModal={isOpenModal}
                toggleModal={toggleModal}
                closeBtnModal={closeBtnModal}
            />
            <ContentFeatureTop
                title={<Fragment> <span className="text-blue">J A L I N</span> SAVING GOALS</Fragment>}
                content="Find the right saving options for your goals"
                image={image1}

                //card
                styling="card-content-bgcolor-lightblue"
                cardTitle1="Jaga Aja"
                cardContent1="Save your money by allocating up to 3 savings and earn 2.5% p.a. interest."
                cardImg1={jaga}
                cardTitle2="Ikut Beraksi"
                cardContent2="Act to make your dreams come true by saving automatically and earn 4% p.a. interest."
                cardImg2={pig}
            />
            <Contentfeature
                styling="content-bgcolor-blue"
                title="RECURRING TRANSACTIONS"
                content="By knowing your recurring transactions, you can make payments automatically for the following month. No need to worry anymore on late bills payment."
                image={image2}
            />
            <Contentfeature
                title="DAILY CHECK IN"
                content="Keep the candle burning by doing check in consecutively to earn more reward points. Collect more points and stand a chance to lead in the leaderboard."
                image={image3}
            />
            <Contentfeature
                styling="content-bgcolor-yellow"
                title="MISSION"
                content="Complete as many missions as you can to compete with others and be the champion in the leaderboard to win more prizes."
                image={image4}
            />
            <Contentfeature
                title="QR CODE PAYMENT"
                content="Simply Scan QR Code for cashless payment."
                image={image5}
            />
            <Contentfeature
                styling="content-bgcolor-lightblue"
                title="TOP UP E-WALLET"
                content="Make your e-wallet list and top up any of it hassle free."
                image={image6}
            />
            <Contentfeature
                title="GRAPHIC MUTATION"
                content="Viewing your bank account mutation has never been this easy. Get your summary and detail report on expenditure and income visualised."
                image={image7}
            />
            <Contentjalin
                styling="content-bgcolor-whiteblue"
                title="Keep the candle burning, so your neighbour might think you do Ngepet Ritual"
                content="Register a Jalin Account to get the latest solution for banking activities."
                button="Get Jalin"
                stylebtn="get-jalin-btn"
                image={candle}
                toggleModal={toggleModal}
            />
            <Footer />
        </>
    )
}

export default Feature