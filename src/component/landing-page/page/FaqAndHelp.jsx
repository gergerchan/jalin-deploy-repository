import { NavLink, Route } from 'react-router-dom';
// import "../component/learnmore/learn-component/learnmore.scss";
import Footer from "../component/footer/Footer"
import TopNavbar from "../component/header/TopNavbar"
import Contentjalin from "../component/content/Contentjalin"
import image6 from "../../../asset/img/home-candle.svg"
import Faq from '../component/faq-and-help/Faq';
import Help from '../component/faq-and-help/Help';

const FaqAndHelp = ({ isOpenModal, toggleModal, closeBtnModal }) => {
    return (
        <div className='base'>
            <TopNavbar
                isOpenModal={isOpenModal}
                toggleModal={toggleModal}
                closeBtnModal={closeBtnModal}
            />

            <div className='container my-5'>
                <div className="row">
                    <div className="col text-center switch-learn">
                        <NavLink to='/help' className="switch-learn-item pb-3 px-lg-5 px-3" activeClassName='switch-learn-item-active'>
                            <span className="b-700 f-25 mb-0">Help Center</span>
                        </NavLink>
                        <NavLink to='/faq' className="switch-learn-item pb-3 px-lg-5 px-3" activeClassName='switch-learn-item-active'>
                            <span className="b-700 f-25 mb-0">F.A.Q</span>
                        </NavLink>
                    </div>
                </div>
            </div>

            <Route exact path='/help'>
                <Help />
            </Route>
            <Route path='/faq'>
                <Faq />
            </Route>

            <Contentjalin
                title="Keep the candle burning, so your neighbour might think you do Ngepet Ritual"
                content="Register a Jalin Account to get the latest solution for banking activities."
                button="Get Jalin"
                stylebtn="get-jalin-btn"
                image={image6}
                toggleModal={toggleModal}
                styling="content-bgcolor-whiteblue"
            />

            <Footer />
        </div>
    )
}

export default FaqAndHelp
