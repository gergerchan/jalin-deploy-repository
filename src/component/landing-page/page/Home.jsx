import { Fragment } from "react"
import Footer from "../component/footer/Footer"
import TopNavbar from "../component/header/TopNavbar"
import Content from "../component/content/Content"
import Contentjalin from "../component/content/Contentjalin"
import image from "../../../asset/img/home-candlelight.svg"
import image1 from "../../../asset/img/home-pig-candle.svg"
import image2 from "../../../asset/img/home-leaderboard.svg"
import image3 from "../../../asset/img/home-time-management.svg"
import image4 from "../../../asset/img/qr-scan 1.svg"
import image5 from "../../../asset/img/home-line-chart.svg"
import image6 from "../../../asset/img/home-candle.svg"
import Logo from "../../../asset/img/Logo-Jalin.png"
import Google from "../../../asset/img/Google-Play.png"
import Getjalin from "../component/content/Getjalin"

const Home = ({ isOpenModal, toggleModal, closeBtnModal }) => {
    return (
        <>
            <TopNavbar
                isOpenModal={isOpenModal}
                toggleModal={toggleModal}
                closeBtnModal={closeBtnModal}
            />
            <Getjalin
                content1="Do more with a Bank that gives you more with every spend and save."
                content2="Make "
                jalin=" J A L I N "
                content3="yours - Apply Now"
                logo={Logo}
                image={image}
                google={Google}
            />
            <Content
                styling="content-bgcolor-blue"
                title={<Fragment>Choose between <span className="content-yellow">JAGA AJA</span> or
                    <span className="content-blue"> IKUT BERAKSI</span> to fullfill your dreams</Fragment>}
                content="Use Jalin Saving Goals to enjoy an attractive interest rate of up to 4% p.a "
                button="Learn More"
                stylebtn="content-btn"
                image={image1}
                link="jaga-aja"
            />
            <Content
                title="It’s time to WIN!"
                content="Earn points as much as you can through Daily Check In & Transactions 
                and compete to win Prizes!"
                button="Learn More"
                stylebtn="content-btn"
                image={image2}
                link="letswin"
            />
            <Content
                styling="content-bgcolor-yellow"
                title="Worry no more on late electricity bill payments!"
                content="Just set up your bill payment to Recurring Transactions and will be automatically deducted
                monthly from your account."
                button="Learn More"
                stylebtn="content-btn-blue"
                image={image3}
                link="recurring"
            />
            <Content
                title="Scan QR for easy payment"
                content="Simply scan QR Code to do a cashless payment."
                button="Learn More"
                stylebtn="content-btn"
                image={image4}
                link="scanqrcode"
            />
            <Content
                styling="content-bgcolor-lightblue"
                title="No more hassle while reading your mutation"
                content="Mutation in graphic format to make it easier for you. "
                button="Learn More"
                stylebtn="content-btn-blue"
                image={image5}
                link="mutation"
            />
            <Contentjalin
                title="Keep the candle burning, so your neighbour might think you do Ngepet Ritual"
                content="Register a Jalin Account to get the latest solution for banking activities."
                button="Get Jalin"
                stylebtn="get-jalin-btn"
                image={image6}
                toggleModal={toggleModal}
            />
            <Footer />
        </>
    )
}

export default Home
