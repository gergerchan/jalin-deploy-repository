import CardPromo from "../component/card-promo/CardPromo"
import Footer from "../component/footer/Footer"
import TopNavbar from "../component/header/TopNavbar"

const Promo = ({isOpenModal, toggleModal, closeBtnModal}) => {
    return (
        <div>
            <TopNavbar
                isOpenModal={isOpenModal} 
                toggleModal={toggleModal}
                closeBtnModal={closeBtnModal} 
            />
            <CardPromo
                toggleModal={toggleModal}
            />
            <Footer />
        </div>
    )
}

export default Promo
