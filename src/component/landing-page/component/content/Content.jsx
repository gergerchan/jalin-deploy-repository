import { Link } from "react-router-dom"
import { Container } from "reactstrap"
import "./content.scss"
const Content = (props) => {
    return (
        <>
            {/* <div className={props.styling}> */}
            <div className={`content py-lg-5 ${props.styling}`}>
                <Container className="py-lg-5">
                    <div className="row">
                        <div className="col-lg-7 mt-5 px-5 px-lg-0 content-text-mobile">
                            <h2 className="b-700 mb-4  mb-lg-0 f-35">{props.title}</h2>
                            <p className="f-20 mb-3 mb-lg-5 ">{props.content}</p>
                            <img src={props.image} className="content-mobile mx-auto" alt="jalin"/>
                            <Link to={props.link} className={`px-5 py-2 mx-5 mx-lg-0 content-web ${props.stylebtn}`}>{props.button}</Link>
                        </div>
                        <div className="col-lg-2"></div>
                        <div className="col-lg-3 content-div">
                            <Link to={props.link} className={`px-5 py-2 mx-5 my-4 content-mobile ${props.stylebtn}`}>{props.button}</Link>
                            <img src={props.image} className="content-web" alt="jalin"/>
                        </div>
                    </div>
                </Container>
            </div>
        </>
    )
}

export default Content
