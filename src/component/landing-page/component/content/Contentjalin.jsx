import { Container } from "reactstrap"
import { Link } from 'react-router-dom';

import "./content.scss"
const Contentjalin = ({styling, image, title, content, stylebtn, button, toggleModal}) => {
    return (
        <>
            <div className={`content-jalin py-3 ${styling}`}>
                <Container className="py-5">
                    <div className="row text-center">
                        <div className="col-lg-12">
                            <img src={image} alt="jalin"/>
                        </div>
                        <div className="col-lg-12 mt-2">
                            <h2 className="b-700 f-35">Keep the candle burning to get more rewards</h2>
                            <p className="f-20 mb-5">{content}</p>
                            <Link onClick={toggleModal} className={`px-4 py-3 get-jalin-btn`}>{button}</Link>
                        </div>
                        <div className="col-lg-12"></div>
                        
                    </div>
                </Container>
            </div>
        </>
    )
}

export default Contentjalin
