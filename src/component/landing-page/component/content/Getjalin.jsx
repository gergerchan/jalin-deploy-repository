import { Container } from "reactstrap"

import "./content.scss"
const Getjalin = (props) => {
    return (
        <>
            <div className={`content pt-lg-5 `}>
            <div className="container-box">
                <div className="box-1"></div>
                <div className="box-2"></div>
            
                <Container className="py-2">
                    <div className="row">
                        <div className="col-lg-1 "></div>
                        <div className="col-lg-6 col-md-5 col-7 p-0 ms-3 ms-md-5 ms-lg-0 mt-4 mt-lg-2 content-text-getjalin">
                            <img src={props.logo} alt="jalinlogo"/>
                            <p className="f-18 mb-4 content-text-align">{props.content1}</p>
                            <p className="b-700 f-18 mb-3 content-text-align">{props.content2}<span className="text-blue">{props.jalin} </span>{props.content3}</p>
                            <a href={process.env.REACT_APP_APK}><img src={props.google} className="content-image" alt="jalin"/></a>
                        </div>
                        <div className="col-lg-1 col-md-0"></div>
                        <div className="col-lg-4 col-md-5  col-4 content-image-position">
                            <img src={props.image} className="content-image" alt="jalin"/>
                        </div>
                    </div>
                </Container>
                </div>
            </div>
        </>
    )
}

export default Getjalin
