import Slider from "react-slick";
import "./promocarousel.scss"
import img1 from "./img/promo-shopping.jpg"
import img2 from "./img/promo-dining.jpg"
import img3 from "./img/promo-travel.jpg"
import PromoItemModals from '../card-promo/PromoItemModals';
import { useState } from 'react';

const PromoCarousel = ({title, image, description, validPeriod, shortDescription, toggleModal }) => {
  const [promoItemModal, setPromoItemModal] = useState(false);
  const [dataPromo, setDataPromo] = useState({title:"Shopee",
  image:"/img/shopee.png",
  shortDescription:"Up to 15% Discount",
  validPeriod:"Valid Till 31/08/2021",
  howTo:"Pay using your JALIN account//Valid for all products with a minimum transaction of Rp 300.000 and limited to a maximum discount of Rp 50.000",
  terms:"Valid for all products with a minimum transaction of Rp 300.000 and limited to a maximum discount of Rp 50.000//Promotion is only valid for non promotion items//Limited to a maximum of 1 transaction per account per day",
  validDetaiil: "Valid from 01 June 2021 to 31 August 2021 ",
})

  const togglePromoItemModal = () => setPromoItemModal(!promoItemModal);
  const closePromoItemBtn = <button className="btn-close" onClick={togglePromoItemModal}></button>;

  var settings = {
    dots: true,
    autoplay: true,
    speed: 1000,
    autoplaySpeed: 4000,
    infinite: true,
    cssEase: "linear",
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows:false,
    customPaging: (i) => (
      <div
        style={{
          bottom:"4%",
          width: "10px",
          height: "10px",
          borderRadius: "20px",
          backgroundColor: "#c4c4c4"
        }}
      ></div>
    )
  };

  const handleModalA = (e) => {
    e.preventDefault()
    setDataPromo({title:"Shopee",
    image:"/img/shopee.png",
    shortDescription:"Up to 15% Discount",
    validPeriod:"Valid Till 31/08/2021",
    howTo:"Pay using your JALIN account//Valid for all products with a minimum transaction of Rp 300.000 and limited to a maximum discount of Rp 50.000",
    terms:"Valid for all products with a minimum transaction of Rp 300.000 and limited to a maximum discount of Rp 50.000//Promotion is only valid for non promotion items//Limited to a maximum of 1 transaction per account per day",
    validDetaiil: "Valid from 01 June 2021 to 31 August 2021 ",
  })
  console.log(e.target.value);
    togglePromoItemModal()
  }
  const handleModalB = (e) => {
    e.preventDefault()
    setDataPromo({title: "Pizza Hut",
    image: "/img/pizza.png",
    shortDescription: "Up to 35% Discount",
    validPeriod: "Valid Till 31/08/2021",
    howTo:"Pay using your JALIN account//Valid for all products with a minimum transaction of Rp 300.000 and limited to a maximum discount of Rp 70.000",
    terms:"Valid for all products with a minimum transaction of Rp 300.000 and limited to a maximum discount of Rp 70.000//Promotion is only valid for non promotion items//Limited to a maximum of 1 transaction per account per day",
    validDetaiil: "Valid from 01 June 2021 to 31 August 2021 ",
  })
  console.log(e.target.value);
    togglePromoItemModal()
  }
  const handleModalC = (e) => {
    e.preventDefault()
    setDataPromo({title: "Ritz Carlton",
    image: "/img/ritz.png",
    shortDescription: "Up to 20% Discount",
    validPeriod: "Valid Till 31/08/2021",
    howTo:"Pay using your JALIN account//Valid for Rooms and F&B with a minimum transaction of Rp 2.000.000 and limited to a maximum discount of Rp 300.000",
    terms:"Valid for Rooms and F&B with a minimum transaction of Rp 2.000.000 and limited to a maximum discount of Rp 300.000//Promotion is only valid for non promotion items//Limited to a maximum of 1 transaction per account per day",
    validDetaiil: "Valid from 01 June 2021 to 31 August 2021 ",
  })
  console.log(e.target.value);
    togglePromoItemModal()
  }

  return (
    <div className="promo-carousel">
        <Slider  {...settings}>
          <div className="carousel-content">
            <img src={img1} alt="" style={{width:"100%"}}/>
            <div className="trapezium trapezium-blue">
            </div>
            <div className="trapezium-text content-text-white">
              <h1 className="b-700">SHOP FROM HOME</h1>
              <h1 className="b-700">TILL DROP</h1>
              <p className="mb-2 mb-lg-5 trapezium-content-web">Deals at your favorite online merchant up to 30 %</p>
              <p className="m-0 trapezium-content-mobile">Deals at your favorite online </p>
              <p className="mb-2 mb-lg-5 trapezium-content-mobile">merchant up to 30 %</p>
              <button className="btn-learnmore-secondary content-text-white px-2 py-1 px-lg-4 py-lg-2" onClick={handleModalA}>Learn More</button>
            </div>
          </div>
          <div className="carousel-content ">
            <img src={img2} alt="" style={{width:"100%"}}/>
            <div className="trapezium trapezium-yellow">
            </div>
            <div className="trapezium-text content-text-black">
              <h1 className="b-700">DINING AT YOUR FAVE</h1>
              <h1 className="b-700">RESTAURANT</h1>
              <p className="mb-2 mb-lg-5 trapezium-content-web">Deals at your favorite restaurant up to 35 %</p>
              <p className="m-0 trapezium-content-mobile">Deals at your favorite online </p>
              <p className="mb-2 mb-lg-5 trapezium-content-mobile">restaurant up to 35 %</p>
              <button className="btn-learnmore-primary content-text-white px-2 py-1 px-lg-4 py-lg-2" onClick={handleModalB}>Learn More</button>
            </div>
          </div>
          <div className="carousel-content ">
            <img src={img3} alt="" style={{width:"100%"}}/>
            <div className="trapezium trapezium-blue">
            </div>
            <div className="trapezium-text content-text-white">
              <h1 className="b-700">LONG WEEKEND?</h1>
              <h1 className="b-700">LET’S STAYCATION</h1>
              <p className="mb-2 mb-lg-5 trapezium-content-web">Deals at your favorite hotel & airlines up to 20 %</p>
              <p className="m-0 trapezium-content-mobile">Deals at your favorite hotel </p>
              <p className="mb-2 mb-lg-5 trapezium-content-mobile">& airlines up to 20 %</p>
              <button className="btn-learnmore-secondary content-text-white px-2 py-1 px-lg-4 py-lg-2" onClick={handleModalC}>Learn More</button>
            </div> 
          </div>
        </Slider>

        <PromoItemModals 
                isOpen={promoItemModal}
                toggle={togglePromoItemModal} 
                closeBtn={closePromoItemBtn}
                imageModal={dataPromo.image}
                titleModal={dataPromo.title}
                descriptionModal={dataPromo.shortDescription}
                toggleModal={toggleModal}
                terms = {dataPromo.terms}
                howTo = {dataPromo.howTo}
                validDetaiil  = {dataPromo.validDetaiil}
            />
      </div>
  )
}

export default PromoCarousel