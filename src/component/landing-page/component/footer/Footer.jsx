import Imggoogleplay from "../../../../asset/img/img-googleplay.svg"
import Ojklogo from "../../../../asset/img/OJK_Logo_2.png"
import LogoJalinWhite from "../../../../asset/img/logo_jalin_white.png"
import { RiInstagramFill } from "react-icons/ri";
import { FaTwitterSquare, FaFacebookSquare } from "react-icons/fa";
import './footer.scss'
import { Link } from "react-router-dom"
const Footer = () => {
    return (
        <div className="container-fluid footer">
            <div className="container">
                <div className="row align-self-stretch text-center align-items-baseline footer-link">
                    <div className="col-lg col-md d-flex flex-column align-items-start footer-download">
                        <div className="text-footer">
                            <a href={process.env.REACT_APP_APK} class="text-decoration-none text-download" ><h4 className="download-text">Download </h4></a>
                            <a href={process.env.REACT_APP_APK} ><img src={LogoJalinWhite} alt="logo_jalin_white" className="logo-jalin" /></a>
                        </div>
                        <div className="google"><a href={process.env.REACT_APP_APK}><img className="img-google" src={Imggoogleplay} alt="googleplay" /></a></div>
                    </div>
                    <div className="col-lg col-md position-relative mb-2">
                        <a href="#link" class="text-decoration-none connect-with-us"><h4 className="b-700  text-link-footer"> Connect with Us</h4></a>
                        <div className="social-icon">
                            <div className="icon-footer"><a href="#link"><RiInstagramFill className="sosmed_icon" size="32px" /></a></div>
                            <div className="icon-footer"><a href="#link"><FaTwitterSquare className="sosmed_icon" size="30px" /></a></div>
                            <div className="icon-footer"><a href="#link"><FaFacebookSquare className="sosmed_icon" size="30px" /></a></div>
                        </div>
                    </div>
                    <div className="col-lg col-md mb-2">
                        <Link to="/help" class="text-decoration-none"><h4 className="b-700 text-link-footer">Help</h4></Link>
                    </div>
                    <div className="col-lg col-md mb-2">
                        <Link to="/faq" class="text-decoration-none"><h4 className="b-700 text-link-footer">F.A.Q</h4></Link>
                    </div>
                    <div className="col-lg col-md mb-2">
                        <Link to="/privacy" class="text-decoration-none"><h4 className="b-700 text-link-footer">Privacy Policy</h4></Link>
                    </div>
                    <div className="col-lg col-md mb-2">
                        <Link to="/terms-and-condition" class="text-decoration-none"><h4 className="b-700 text-link-footer">Terms & Condition</h4></Link>
                    </div>
                </div>
                <div className="ojk-copyright-container">
                    <div className="d-inline-block ojk-logo-container">
                        <img className="ojk-logo" src={Ojklogo} alt="" />
                    </div>
                    <div className="d-inline-block copyright">
                        <p className="copyright-text">Copyright © 2021 PT Bank Jalin</p>
                        <p className="copyright-text"> www.bankjalin.com</p>
                        <p className="copyright-text">PT Bank Jalin adalah lembaga perbankan yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan <br />
                            serta merupakan peserta penjaminan Lembaga Penjamin Simpanan (LPS).</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer
