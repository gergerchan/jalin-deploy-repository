import { Modal, ModalHeader } from 'reactstrap';
import Imggoogleplay from "../../../../asset/img/googleplay.png";
import QrcodeJalin from "../../../../asset/img/qrcodejalin.svg";
import getjalinImgModal from "../../../../asset/img/getjalin-img-modal.png";
import "./qrcodeModals.scss";

const QrcodeModals = ({isOpen, toggle, closeBtnModal}) => {
    return (
        <>
            <Modal className='base modal-xl-custom' size='xl' isOpen={isOpen} toggle={toggle}>
                <ModalHeader className='border-bottom-0 p-0' toggle={toggle} close={closeBtnModal}>
                    <div className='modal-header-row row'>
                        <div className="modal-header-left-side col-lg-6 col-12">
                            <h3 className='b-700'>Anyone can JALIN to get CUAN</h3>
                            <p className='mb-5' style={{lineHeight: "18px"}}>
                                Start to use a Digital Banking that makes your life easier. Download JALIN and
                                register your account from your phone. 
                            </p>
                            <a href={process.env.REACT_APP_APK}><img src={Imggoogleplay}  alt="download google play" /></a>
                            <p className='my-4'>Scan this QR code with your smartphone's camera to get JALIN.</p>
                            <img className='py-2 qrcode-image' src={QrcodeJalin}  alt="QR Code Jalin" />
                        </div>
                        <div className="modal-header-right-side col-lg-6 col-12">
                            <img src={getjalinImgModal} alt="get jalin modal" />
                        </div>
                    </div>
                </ModalHeader>
            </Modal>
        </>
    )
}

export default QrcodeModals
