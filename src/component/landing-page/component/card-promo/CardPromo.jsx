import React, { useState } from "react";
import { NavLink, Route } from 'react-router-dom';
import Fuse from "fuse.js";
import "./cardPromo.scss";
import promos from "../../../../asset/json/PromoList.json";
import PromoItem from "./PromoItem";
import SearchBar from "./SearchBar";
import olshopIcon from '../../../../asset/img/olshop-icon.svg';
import diningIcon from '../../../../asset/img/dining-icon.png';
import travelIcon from '../../../../asset/img/travel-icon.png';
import theaterIcon from '../../../../asset/img/theater-icon.svg';
import OlshopPromo from "./OlshopPromo";
import DiningPromo from "./DiningPromo";
import TravelPromo from "./TravelPromo";
import EntertainPromo from "./EntertainPromo";
import PromoCarousel from "../promo-carousel/PromoCarousel"

const CardPromo = ({ toggleModal }) => {
    const [data, setData] = useState(promos);

    const dataOlshopPromo = promos.filter(function(promos) {
        return promos.kategori === 'olshop'
    });

    const dataDiningPromo = promos.filter(function(promos) {
        return promos.kategori === 'dining'
    });

    const dataTravelPromo = promos.filter(function(promos) {
        return promos.kategori === 'travel'
    });

    const dataEntertainPromo = promos.filter(function(promos) {
        return promos.kategori === 'entertain'
    });

    const searchData = (pattern) => {
        if (!pattern) {
            setData(promos);
            return;
        }

        const fuse = new Fuse(data, {
            keys: ["title", "description"],
        });
        
        const result = fuse.search(pattern);
        const matches = [];

        if (!result.length) {
            setData([]);
        } else {
            result.forEach(({item}) => {
            matches.push(item);
        });
            setData(matches);
        }
    };

    return (
        <>
            <PromoCarousel />
            <div className='base mt-5 mb-5'>
                <div className='card-promo container'>
                    <div className="row card-promo-title">
                        <h3 className="card-promo-title-header f-30 b-700">Best deals at your fingertips!</h3>
                        <p className="card-promo-title-subheader b-600 mb-0">Over 100 deals at your favourite brands with Jalin Account</p>
                    </div>
                    <div className="row card-promo-nav">
                        <div className="card-promo-nav-icon col-6 col-lg-3 p-0 d-flex justify-content-center">
                            <NavLink to='/promo/online-shop' className="card-promo-nav-icon-item text-center" activeClassName='card-promo-nav-active'>
                                <img src={olshopIcon} alt="online shop icon" />
                                <p className="card-promo-nav-icon-item-title b-600 f-18 mt-3 mb-0">Online Shopping</p>
                            </NavLink>
                        </div>
                        <div className="card-promo-nav-icon col-6 col-lg-3 p-0 d-flex justify-content-center">
                            <NavLink to='/promo/dining' className="card-promo-nav-icon-item text-center" activeClassName='card-promo-nav-active'>
                                <img src={diningIcon} alt="dining icon" />
                                <p className="card-promo-nav-icon-item-title b-600 f-18 mt-3 mb-0">Dining</p>
                            </NavLink>
                        </div>
                        <div className="card-promo-nav-icon col-6 col-lg-3 p-0 d-flex justify-content-center">
                            <NavLink to='/promo/travel'className="card-promo-nav-icon-item text-center" activeClassName='card-promo-nav-active'>
                                <img src={travelIcon} alt="travel icon" />
                                <p className="card-promo-nav-icon-item-title b-600 f-18 mt-3 mb-0">Travel</p>
                            </NavLink>
                        </div>
                        <div className="card-promo-nav-icon col-6 col-lg-3 p-0 d-flex justify-content-center">
                            <NavLink to='/promo/entertainment'className="card-promo-nav-icon-item text-center" activeClassName='card-promo-nav-active'>
                                <img src={theaterIcon} alt="theater icon" />
                                <p className="card-promo-nav-icon-item-title b-600 f-18 mt-3 mb-0">Entertainment</p>
                            </NavLink>
                        </div>
                    </div>
                </div>

                <hr className='card-promo-bottom-line mt-0'/>

                <Route exact path='/promo'>
                    <SearchBar 
                        placeholder="Search" 
                        onChange={(e) => searchData(e.target.value)}
                    />

                    <div className='promo-list container'>
                        {data.map((item) => (
                            <PromoItem {...item} key={item.title} toggleModal={toggleModal}/>
                        ))}
                    </div>
                </Route>
                <Route exact path='/promo/online-shop'>
                    <OlshopPromo toggleModal={toggleModal} dataOlshopPromo={dataOlshopPromo} allPromo={promos}/>
                </Route>
                <Route exact path='/promo/dining'>
                    <DiningPromo toggleModal={toggleModal} dataDiningPromo={dataDiningPromo} allPromo={promos}/>
                </Route>
                <Route exact path='/promo/travel'>
                    <TravelPromo toggleModal={toggleModal} dataTravelPromo={dataTravelPromo} allPromo={promos}/>
                </Route>
                <Route exact path='/promo/entertainment'>
                    <EntertainPromo toggleModal={toggleModal} dataEntertainPromo={dataEntertainPromo} allPromo={promos}/>
                </Route>
            </div>
        </>
    )
}

export default CardPromo