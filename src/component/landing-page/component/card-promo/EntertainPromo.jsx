import React, { useState } from "react";
import Fuse from "fuse.js";
import PromoItem from "./PromoItem";
import SearchBar from "./SearchBar";

const EntertainPromo = ({dataEntertainPromo, toggleModal, allPromo}) => {
    const [data, setData] = useState(dataEntertainPromo);
    const allDataPromo = allPromo;

    const searchData = (pattern) => {
        if (!pattern) {
            setData(dataEntertainPromo);
            return;
        }

        const fuse = new Fuse(allDataPromo, {
            keys: ["title", "description"],
        });
        
        const result = fuse.search(pattern);
        // console.log(result);
        const matches = [];

        if (!result.length) {
            setData([]);
        } else {
            result.forEach(({item}) => {
            matches.push(item);
        });
            setData(matches);
        }
    };

    return (
        <div>
            <SearchBar 
                placeholder="Search" 
                onChange={(e) => searchData(e.target.value)}
            />

            <div className='promo-list container'>
                {data.map((item) => (
                    <PromoItem {...item} key={item.title} toggleModal={toggleModal}/>
                ))}
            </div>
        </div>
    )
}

export default EntertainPromo