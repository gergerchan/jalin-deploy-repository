import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Link } from 'react-router-dom';
import "./modals.scss";

const OlshopModals = ({isOpen, toggle, closeBtn, imageModal, titleModal, descriptionModal, toggleModal, validDetail,howTo,terms }) => {
    const howstring = howTo.split("//")
    const termsstring = terms.split("//")
    
    return (
        <div>
            <Modal className='base' size='lg' isOpen={isOpen} toggle={toggle}>
                <ModalHeader className='p-0 border-bottom-0' toggle={toggle} close={closeBtn}>
                    <img className='promo-img-modal' src={imageModal} alt={titleModal} />
                </ModalHeader>
                <ModalBody className='px-5 pt-5 pb-4'>
                    <h1 className='b-700 mb-2'>{titleModal}</h1>
                    <h4 className='promo-desc b-700 mb-4'>{descriptionModal}</h4>
                    <h4 className='b-700 mb-3'>How to enjoy:</h4>
                    <ul className='px-4' style={{lineHeight: '18px', maxWidth: '580px'}}>
                        {howstring.map((data,i) => 
                            <li>{data}</li>
                        )}
                    </ul>
                    <p className='promo-valid-period mb-4 px-4'>{validDetail}</p>
                    <p className='promo-detail-header b-700'>Terms & Condition</p>
                    <ol className='px-4' style={{lineHeight: '18px', maxWidth: '580px'}}>
                        {termsstring.map((data) => 
                            <li>{data}</li>
                        )}
                    </ol>
                </ModalBody>
                <ModalFooter>
                    <h1 className='modal-footer-title b-800 mb-4'>Keen to enjoy these promotions?</h1>
                    <h4 className='modal-footer-subtitle mb-5' style={{color: 'white'}}>With Jalin Account, it’s easy to enjoy all of life’s privileges.</h4>
                    <Link onClick={toggleModal} className="modal-footer-nav-btn py-2 px-3">Get Jalin</Link>
                </ModalFooter>
            </Modal>
        </div>
    )
}

export default OlshopModals
