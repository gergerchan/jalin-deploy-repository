import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import PromoItemModals from './PromoItemModals';

const PromoItem = ({title, image, description, validPeriod, shortDescription, terms,howTo,validDetaiil, toggleModal }) => {
    const [promoItemModal, setPromoItemModal] = useState(false);

    const togglePromoItemModal = () => setPromoItemModal(!promoItemModal);

    const closePromoItemBtn = <button className="btn-close" onClick={togglePromoItemModal}></button>;

    return (
        <div onClick={togglePromoItemModal} className='base promo-item promo-item-hover'>
            <div className="col-promo-item-img">
                <Link >
                    <img className='promo-item-img' style={{width: "250px", Height: "200px"}} src={image} alt={title} /> 
                </Link>
            </div>
            <div className="col-promo-item-detail" style={{maxWidth: '216px'}}>
                <Link className='text-decoration-none'>
                    <h5 className='b-700' style={{color: '#000000'}}>{title}</h5>
                    <p className='mb-2' style={{color: '#000000', lineHeight: '19px'}}>{description}</p>
                    <p className='b-600' style={{color: '#BFBFBF'}}>{validPeriod}</p>
                </Link>
            </div>

            <PromoItemModals 
                isOpen={promoItemModal}
                toggle={togglePromoItemModal} 
                closeBtn={closePromoItemBtn}
                imageModal={image}
                titleModal={title}
                descriptionModal={shortDescription}
                validPeriod={validPeriod}
                toggleModal={toggleModal}
                terms = {terms}
                howTo = {howTo}
                validDetaiil  = {validDetaiil}
            />
        </div>
    )
}

export default PromoItem
