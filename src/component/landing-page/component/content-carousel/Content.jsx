import { Container } from "reactstrap"
import ContentCarousel from "./ContentCarousel"
const Content = () => {
    return (
        <div className="content-carousel">
            <Container className="mt-4 mb-5 py-3">
                <div className="row">
                    <div className="col-lg-1"></div>
                    <div className="col-lg-10">
                        <h2 className="mb-5 b-700">How To</h2>
                        <div className="carousel-card">
                            <ContentCarousel/>
                        </div>
                    </div>
                    <div className="col-lg-1"></div>
                </div>
            </Container>
        </div>
    )
}

export default Content
