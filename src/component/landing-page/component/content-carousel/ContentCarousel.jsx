import { useState } from "react";
import Slider from "react-slick";
import "./contentcarousel.scss"
import img1 from "./img/feature-checkin.png"
import img2 from "./img/feature-scanqr.png"
const ContentCarousel = (props) => {
  const [slides, setSlides] = useState([1, 2]);
  const [data, setData] = useState([
    ["Step 1","On your Home tab, Check In as you click on the coin available that day.",img1],
    ["Step 2","Your Check In is Successful! Come back tomorrow for more reward points.",img2]
  ])
  const [button, setButton] = useState("left")

  const create = () => {
    setSlides([1, 2])
    setData([
      ["Step 1","On your Home tab, Check In as you click on the coin available that day.",img1],
      ["Step 2","Your Check In is Successful! Come back tomorrow for more reward points.",img2]
    ])
    setButton("left")
  }
  const pause = () => {
    setSlides([1, 2])
    setData([
      ["Step 1","On your Reward tab, Select Mission.",img1],
      ["Step 2","Select on Weekly, Biweekly, or Monthly to see the missions and its Progress. When the mission is accomplished, you can Collect Point.",img2]
    ])
    setButton("middle")
  }
  const stop = () => {
    setSlides([1, 2])
    setData([
      ["Step 1","On your Reward tab, Select Redeem Point.",img1],
      ["Step 2","Claim the vouchers that you want.",img2]
    ])
    setButton("right")
  }
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    customPaging: (i) => (
      <div
        style={{
          left: "10px",
          width: "10px",
          height: "10px",
          borderRadius: "20px",
          backgroundColor: "#c4c4c4"
        }}
      ></div>
    ),
  };
  return (
    <div className="content-carousel p-5">
      {/* <div className="mb-4">
        {button === "left" ?  
          <button className="px-3 py-1 btn-carousel-left btn-carousel-active" onClick={create}>
            Create
          </button>:
            <button className="px-3 py-1 btn-carousel-left " onClick={create}>
            Create
          </button>
        }
        {button === "middle" ?  
          <button className="px-3 py-1 btn-carousel-middle btn-carousel-active" onClick={pause}>
            Pause
          </button> :
          <button className="px-3 py-1 btn-carousel-middle" onClick={pause}>
            Pause
          </button>
        }
        {button === "right" ?  
          <button className="px-3 py-1 btn-carousel-right btn-carousel-active" onClick={stop}>
            Stop
          </button> :
          <button className="px-3 py-1 btn-carousel-right" onClick={stop}>
            Stop
          </button>
        }
      </div> */}
      
      {/* <Slider {...settings}>
        {slides.map(function(slide) {
          return (
            <div key={slide}>
              <div className="row ">
                <div className="col-lg-8 ">
                  <h3 style={{fontSize:"20px"}}>{data[slide-1][0]}</h3>
                  <h3 style={{fontSize:"20px"}} className="b-700">{data[slide-1][1]}</h3>
                </div>
                
              <div className="col-lg-1"></div>
                <div className="col-lg-3 ">
                  <img src={data[slide-1][2]} alt="" className="carousel-img"/>
                </div>
              </div>
            </div>
          );
        })}
      </Slider> */}

      <Slider {...settings}>
        {slides.map(function(slide) {
          return (
            <div className="content-carousel-card" key={slide}>
              <div className="text-box p-5">
                  <h3 style={{fontSize:"20px"}}>{data[slide-1][0]}</h3>
                  <h3 style={{fontSize:"20px"}} className="b-700">{data[slide-1][1]}</h3>
              </div>
              <div className="img-box">
                <img src={data[slide-1][2]} alt=""/>
              </div>
            </div>
          );
        })}
      </Slider>
    </div>
  )
}

export default ContentCarousel
