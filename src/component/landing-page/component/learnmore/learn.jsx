import { NavLink, Route } from 'react-router-dom';
import "./learn-component/learnmore.scss";
import JagaAja from "./learn-jaga"
import Beraksi from './learn-beraksi';

const LearnMore = ({isOpenModal, toggleModal, closeBtnModal}) => {

    return (
        <div className='base mt-lg-5 mt-3 justify-text-center'>
            <div className='container'>
                <div className="row">
                    <div className="col text-center switch-learn">
                        <NavLink to='/jaga-aja' className="switch-learn-item pb-3 px-lg-5 px-3" activeClassName='switch-learn-item-active'>
                            <span className="b-700 f-25 mb-0">Jaga Aja</span>
                        </NavLink>
                        <NavLink to='/beraksi' className="switch-learn-item pb-3 px-lg-5 px-3" activeClassName='switch-learn-item-active'>
                            <span className="b-700 f-25 mb-0">Ikut Beraksi</span>
                        </NavLink>
                    </div>
                </div>
            </div>

            <Route exact path='/beraksi'>
                <Beraksi 
                isOpenModal={isOpenModal} 
                toggleModal={toggleModal}
                closeBtnModal={closeBtnModal}/>
            </Route>
            <Route exact path='/jaga-aja'>
                <JagaAja 
                isOpenModal={isOpenModal} 
                toggleModal={toggleModal}
                closeBtnModal={closeBtnModal}/>
            </Route>
            
        </div>
    )
}

export default LearnMore