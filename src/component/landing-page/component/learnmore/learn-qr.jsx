import Footer from "../footer/Footer"
import TopNavbar from "../header/TopNavbar"
import LearnRight from "./learn-component/learn-right"
import LearnLeft from "./learn-component/learn-left"
import Contentjalin from "../content/Contentjalin"
import Carousel from "./carousel/qr-carousel"
import scan from "../../../../asset/img/qr-scan 1.svg"
import image1 from "../../../../asset/img/feature-image5.png"
import candle from "../../../../asset/img/home-candle.svg"
import LearnPartners from "./learn-component/learn-ourpartner"


const LearnQR = ({isOpenModal, toggleModal, closeBtnModal}) => {
    return (
        <>
        <TopNavbar
                isOpenModal={isOpenModal} 
                toggleModal={toggleModal}
                closeBtnModal={closeBtnModal}
            />
        <LearnRight 
            titleTop="QR Code Payment"
            title="Scan QR Code"
            content="Earn as many points as you can to compete and be a champion in the leaderboard."
            image={scan}
            titletype={true}
        />
        <LearnLeft
            styling="content-bgcolor-lightblue"
            title="Instant Payments"
            content2="Pay for your purchases at merchant outlets by simply scan QR Code using your Jalin’s App."
            image={image1}
        />
        <LearnPartners/>
        <Carousel/>
        <Contentjalin 
            title="Keep the candle burning, so your neighbour might think you do Ngepet Ritual"
            content="Register a Jalin Account to get the latest solution for banking activities."
            button="Get Jalin"
            stylebtn="content-btn"
            image={candle}
            toggleModal={toggleModal}
        />
        <Footer/>
        </>
    )
}

export default LearnQR;