import Footer from "../footer/Footer"
import TopNavbar from "../header/TopNavbar"
import LearnRight from "./learn-component/learn-right"
import LearnLeft from "./learn-component/learn-left"
import Contentjalin from "../content/Contentjalin"
import Carousel from "./carousel/recurring-carousel"
import img1 from "../../../../asset/img/recurring-img1.svg"
import img2 from "../../../../asset/img/recurring-img2.png"
import candle from "../../../../asset/img/home-candle.svg"
import LearnPartners2 from "./learn-component/learn-ourpartner2"


const LearnRecurring = ({isOpenModal, toggleModal, closeBtnModal}) => {
    return (
        <>
        <TopNavbar
                isOpenModal={isOpenModal} 
                toggleModal={toggleModal}
                closeBtnModal={closeBtnModal}
            />
        <LearnRight 
            titleTop="Recurring Transaction"
            title="Bank Transfer, Top-up E-Wallet, and Billing & Internet"
            content="Offer to you the hassle-free feature for your recurring needs. Just few clicks away and it is done. "
            image={img1}
            titletype={true}
        />
        <LearnLeft
            styling="content-bgcolor-lightblue"
            title="Automated Transactions"
            content2="Set schedules for your bank transfer, top up e-wallet or bill payments and no more late transactions. Dealing with recurring payments is now getting simpler!"
            image={img2}
        />
        <LearnPartners2/>
        <Carousel/>
        <Contentjalin 
            title="Keep the candle burning, so your neighbour might think you do Ngepet Ritual"
            content="Register a Jalin Account to get the latest solution for banking activities."
            button="Get Jalin"
            stylebtn="content-btn"
            image={candle}
            toggleModal={toggleModal}
        />
        <Footer/>
        </>
    )
}

export default LearnRecurring