import Footer from "../footer/Footer"
import TopNavbar from "../header/TopNavbar"
import LearnRight from "./learn-component/learn-right"
import LearnLeft from "./learn-component/learn-left"
import Contentjalin from "../content/Contentjalin"
import Carousel from "./carousel/win-carousel"
import rank from "../../../../asset/img/learnmore-rank.svg"
import image1 from "../../../../asset/img/win-phone1.png"
import image2 from "../../../../asset/img/win-phone2.png"
import image3 from "../../../../asset/img/win-phone3.png"
import candle from "../../../../asset/img/home-candle.svg"

const LetsWin = ({isOpenModal, toggleModal, closeBtnModal}) => {
    return (
        <>
        <TopNavbar
                isOpenModal={isOpenModal} 
                toggleModal={toggleModal}
                closeBtnModal={closeBtnModal}
            />
        <LearnRight 
            titleTop="Daily Check In & Missions"
            title="It’s Time to WIN"
            content="Earn as many points as you can to compete and be a champion in the leaderboard."
            image={rank}
            titletype={true}
        />
        <LearnLeft
            styling="content-bgcolor-lightblue"
            title="Check In Daily to get more rewards points"
            content="Keep the candle burning by doing check in consecutively to earn more reward points." 
            content2="*If Check-in is not consecutive, computation will restart from the initial value."
            image={image1}
        />
        <LearnRight 
            title="Choose your missions"
            content="Complete the missions to earn points. You can choose between Weekly, Biweekly, Monthly missions or do all missions at the same time to collect more points."
            image={image2}
        />
        <LearnLeft
            styling="content-bgcolor-lightblue"
            title="Be the champion!"
            content="Collect more points and stand a chance to lead in the leaderboard."
            image={image3}
        />
        <Carousel/>
        <Contentjalin 
            styling="content-bgcolor-whiteblue"
            title="Keep the candle burning, so your neighbour might think you do Ngepet Ritual"
            content="Register a Jalin Account to get the latest solution for banking activities."
            button="Get Jalin"
            stylebtn="content-btn"
            image={candle}
            toggleModal={toggleModal}
        />
        <Footer/>
        </>
    )
}

export default LetsWin;