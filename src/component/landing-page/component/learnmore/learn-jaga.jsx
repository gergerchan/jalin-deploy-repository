import LearnLeft from "./learn-component/learn-left"
import LearnRight from "./learn-component/learn-right"
import Contentjalin from "../content/Contentjalin"
import candle from "../../../../asset/img/learnmore-candle.svg"
import lilin from "../../../../asset/img/home-candle.svg"
import image1 from "../../../../asset/img/jaga-phone1.png"
import JagaCarousel from "./carousel/jaga-carousel"

const JagaAja = ({toggleModal}) => {
    return (
        <>
        <LearnRight 
            styling="mb-5"
            titleTop="Saving Goals"
            title="Jaga Aja"
            content="Easily allocate your money up to 3 flexible savings. You can deposit / withdraw money from your savings at any time without penalty. With only min. Deposit Rp 500.000, you can enjoy 2.5% p.a interest."
            image={candle}
            titletype={true}
        />
        <LearnLeft
            styling="content-bgcolor-lightblue"
            title="Personalize your saving goals "
            content="Every savings you’ve created in Jaga Aja can be named according to its category and purpose. Add photos to curb your enthusiasm in saving money."
            image={image1}
        />
        <JagaCarousel/>
        <Contentjalin 
            styling="content-bgcolor-whiteblue"
            title="Keep the candle burning, so your neighbour might think you do Ngepet Ritual"
            content="Register a Jalin Account to get the latest solution for banking activities."
            button="Get Jalin"
            stylebtn="content-btn"
            image={lilin}
            toggleModal={toggleModal}
        />
        </>
    )
}

export default JagaAja