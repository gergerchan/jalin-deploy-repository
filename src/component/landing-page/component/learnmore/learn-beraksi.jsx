import LearnLeft2 from "./learn-component/learn-left2"
import LearnRight from "./learn-component/learn-right"
import Contentjalin from "../content/Contentjalin"
import pig from "../../../../asset/img/learnmore-pig.svg"
import image1 from "../../../../asset/img/beraksi-phone1.png"
import image2 from "../../../../asset/img/beraksi-phone2.png"
import money from "../../../../asset/img/learnmore-money.svg"
import lilin from "../../../../asset/img/home-candle.svg"
import BeraksiCarousel from "./carousel/beraksi-carousel"

const Beraksi = ({toggleModal}) => {
    return (
        <>
        <LearnRight 
            styling="mb-5"
            titleTop="Saving Goals"
            title="Ikut Beraksi"
            content="Bigger dreams need more commitment. Allocate your money to unlimited savings. With min. Deposit Rp 10.000.000 per month, you can enjoy 4% p.a interest."
            image={pig}
            titletype={true}
        />
        <LearnLeft2
            styling="content-bgcolor-lightblue"
            title="You decide your choices"
            content="Every savings you’ve created in Ikut Beraksi can be named according to its category and purpose. Add photos to curb your enthusiasm in saving money. Set your saving target, frequency, and lock targeted date to be collected automatically from your account. "
            image={image1}
            image2={image2}
        />
        <LearnRight 
            title="No penalty fees for stopping or pausing your savings"
            content="When you need emergency funds, you can rest easy as you won’t be charged penalty fees for stopping your savings. The current balance (principle value + interests) will be returned to your account. You can even pause your Ikut Beraksi Goals and continue for savings whenever you want."
            image={money}
        />
        <BeraksiCarousel/>
        <Contentjalin 
            title="Keep the candle burning, so your neighbour might think you do Ngepet Ritual"
            content="Register a Jalin Account to get the latest solution for banking activities."
            button="Get Jalin"
            stylebtn="content-btn"
            image={lilin}
            toggleModal={toggleModal}
        />
        </>
    )
}

export default Beraksi