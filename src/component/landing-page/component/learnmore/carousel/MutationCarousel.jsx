import Slider from "react-slick";
import "./contentcarousel.scss"
import img1 from "../../../../../asset/img/mutation-phone1.png"
import img2 from "../../../../../asset/img/mutation-phone2.png"
import img3 from "../../../../../asset/img/mutation-phone3.png"

const RecurringCarousel = (props) => {
  const slides = [1, 2, 3];
  const data = [
    ["Step 1","On your Profile tab, click on History.",img1],
    ["Step 2","You can view your summary of income and expenditure in graphic format and set the period flexibly.",img2],
    ["Step 3","Click on Details to view the details of each income and expenditure. ",img3],
  ]

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    customPaging: (i) => (
      <div
        style={{
          left: "10px",
          width: "10px",
          height: "10px",
          borderRadius: "20px",
          backgroundColor: "#c4c4c4"
        }}
      ></div>
    ),responsive: [
      {
        breakpoint: 1023,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
          arrows:false,
          customPaging: (i) => (
            <div
              style={{
                left: "10px",
                width: "10px",
                height: "10px",
                borderRadius: "20px",
                backgroundColor: "#c4c4c4"
              }}
            ></div>
          ),
        }
      }
    ]
  };
  return (
    <div className="content-carousel content-carousel-mobile">
    
      <Slider {...settings}>
        {slides.map(function (slide) {
          return (
            <div className="content-carousel-card" key={slide}>
              <div className="card-content-wrapper px-5 py-3">
                {" "}
                {/* Add this wrapper to handle the style of the item */}
                <div className="text-box px-3">
                  <h3 style={{ fontSize: "20px" }}>{data[slide - 1][0]}</h3>
                  <h3 style={{ fontSize: "20px" }} className="b-700">
                    {data[slide - 1][1]}
                  </h3>
                </div>
                <div className="img-box px-3">
                  <img
                    src={data[slide - 1][2]}
                    alt=""
                  />
                </div>
              </div>
            </div>
          );
        })}
      </Slider>
    
    </div>
  )
}

export default RecurringCarousel
