import Slider from "react-slick";
import "./contentcarousel.scss"
import img1 from "../../../../../asset/img/beraksi-phone3.png"
import img2 from "../../../../../asset/img/jaga-phone3.png"

const ContentCarousel = (props) => {
  const slides = [1, 2, 3];
  const data = [
    ["Step 1","On your Savings tab, Click on Create New Goals and select Jaga Aja to create savings.",img1],
    ["Step 2","Name your savings to its category, purpose, add photos and set your desired amount.",img2],
    ["Step 3","Tap Create Saving button and you’re done!",img2]
  ]
  
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    customPaging: (i) => (
      <div
        style={{
          left: "10px",
          width: "10px",
          height: "10px",
          borderRadius: "20px",
          backgroundColor: "#c4c4c4"
        }}
      ></div>
    ),responsive: [
      {
        breakpoint: 1023,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
          arrows:false,
          customPaging: (i) => (
            <div
              style={{
                left: "10px",
                width: "10px",
                height: "10px",
                borderRadius: "20px",
                backgroundColor: "#c4c4c4"
              }}
            ></div>
          ),
        }
      }
    ]
  };
  return (
    <div className="content-carousel content-carousel-mobile">
      <Slider {...settings}>
        {slides.map(function (slide) {
          return (
            <div className="content-carousel-card" key={slide}>
              <div className="card-content-wrapper px-5 py-3">
                {" "}
                {/* Add this wrapper to handle the style of the item */}
                <div className="text-box px-3">
                  <h3 style={{ fontSize: "20px" }}>{data[slide - 1][0]}</h3>
                  <h3 style={{ fontSize: "20px" }} className="b-700">
                    {data[slide - 1][1]}
                  </h3>
                </div>
                <div className="img-box px-3">
                  <img
                    src={data[slide - 1][2]}
                    alt=""
                  />
                </div>
              </div>
            </div>
          );
        })}
      </Slider>
    
    </div>
  )
}

export default ContentCarousel
