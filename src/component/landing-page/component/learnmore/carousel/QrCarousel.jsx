import Slider from "react-slick";
import "./contentcarousel.scss"
import img1 from "../../../../../asset/img/wincarousel-phone1.png"
import img2 from "../../../../../asset/img/feature-image5.png"

const QrCarousel = (props) => {
  const slides = [1, 2];
  const data = [
    ["Step 1","On your Home tab, Click on Scan QR Code.",img1],
    ["Step 2","You can directly scan merchant’s QR Code for the payment.",img2],
  ]
  
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    customPaging: (i) => (
      <div
        style={{
          left: "10px",
          width: "10px",
          height: "10px",
          borderRadius: "20px",
          backgroundColor: "#c4c4c4"
        }}
      ></div>
    ),responsive: [
      {
        breakpoint: 1023,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
          arrows:false,
          customPaging: (i) => (
            <div
              style={{
                left: "10px",
                width: "10px",
                height: "10px",
                borderRadius: "20px",
                backgroundColor: "#c4c4c4"
              }}
            ></div>
          ),
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
          arrows:false,
          customPaging: (i) => (
            <div
              style={{
                left: "10px",
                width: "10px",
                height: "10px",
                borderRadius: "20px",
                backgroundColor: "#c4c4c4"
              }}
            ></div>
          ),
        }
      }
    ]
  };
  return (
    <div className="content-carousel content-carousel-mobile">

      <Slider {...settings}>
        {slides.map(function (slide) {
          return (
            <div className="content-carousel-card qr-carousel" key={slide}>
              <div className="card-content-wrapper px-5 py-3">
                {" "}
                {/* Add this wrapper to handle the style of the item */}
                <div className="text-box px-3">
                  <h3 style={{ fontSize: "20px" }}>{data[slide - 1][0]}</h3>
                  <h3 style={{ fontSize: "20px" }} className="b-700">
                    {data[slide - 1][1]}
                  </h3>
                </div>
                <div className="img-box px-3">
                  <img
                    src={data[slide - 1][2]}
                    alt=""
                  />
                </div>
              </div>
            </div>
          );
        })}
      </Slider>
    
    </div>
  )
}

export default QrCarousel
