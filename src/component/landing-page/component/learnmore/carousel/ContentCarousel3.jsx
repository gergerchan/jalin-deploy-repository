import { useState } from "react";
import Slider from "react-slick";
import "./contentcarousel.scss"
import img1 from "../../../../../asset/img/beraksi-phone3.png"
import img2 from "../../../../../asset/img/beraksi-phone1.png"
import img3 from "../../../../../asset/img/beraksi-phone4.png"
import img4 from "../../../../../asset/img/beraksi-phone5.png"
import img5 from "../../../../../asset/img/beraksi-phone2.png"
import img6 from "../../../../../asset/img/beraksi-phone6.png"
import img7 from "../../../../../asset/img/beraksi-phone7.png"
import img8 from "../../../../../asset/img/beraksi-phone8.png"
import img9 from "../../../../../asset/img/beraksi-phone9.png"
import img10 from "../../../../../asset/img/beraksi-phone10.png"
import img11 from "../../../../../asset/img/beraksi-phone11.png"

const ContentCarousel = (props) => {
  const [slides, setSlides] = useState([1, 2, 3]);
  const [data, setData] = useState([
    ["Step 1","On your Savings tab, Click on Create New Goals and select Ikut Beraksi to create savings.",img1],
    ["Step 2","Name your savings to its category, purpose, add photos, set your saving target, frequency, and lock targeted date to be collected automatically from your account.",img2],
    ["Step 3","Tap Create Saving button and you’re done!",img5]
  ])
  const [button, setButton] = useState("left")

  const create = () => {
    setSlides([1, 2, 3])
    setData([
      ["Step 1","On your Savings tab, Click on Create New Goals and select Ikut Beraksi to create savings.",img1],
      ["Step 2","Name your savings to its category, purpose, add photos, set your saving target, frequency, and lock targeted date to be collected automatically from your account.",img2],
      ["Step 3","Tap Create Saving button and you’re done!",img3]
  ])
    setButton("left")
  }
  const pause = () => {
    setSlides([1, 2, 3 , 4, 5])
    setData([
      ["Step 1","On your Savings tab, Select your created Ikut Beraksi Goals. ",img11],
      ["Step 2","On your created Ikut Beraksi Goals Details page, tap the 3 dots button on the upper right corner of your screen.",img4],
      ["Step 3","Tap on Pause Saving. ",img6],
      ["Step 4","Choose Pause to pause your saving.",img7],
      ["Step 5","Now, your saving is paused! You can continue your saving anytime by clicking on Re-activate button.",img9]
    ])
    setButton("middle")
  }
  const stop = () => {
    setSlides([1, 2, 3 , 4, 5])
    setData([
      ["Step 1","On your Savings tab, Select your created Ikut Beraksi Goals.",img11],
      ["Step 2","On your created Ikut Beraksi Goals Details page, tap the 3 dots button on the upper right corner of your screen.",img4],
      ["Step 3","Tap on Stop Saving.",img6],
      ["Step 4","Choose Stop to stop your saving.",img8],
      ["Step 5","Now, your saving is stopped! The total amount in the saving will be returned to your account balance without any interest. You can view the history by clicking on Transaction. ",img10]
    ])
    setButton("right")
  }
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    customPaging: (i) => (
      <div
        style={{
          left: "10px",
          width: "10px",
          height: "10px",
          borderRadius: "20px",
          backgroundColor: "#c4c4c4"
        }}
      ></div>
    ),responsive: [
      {
        breakpoint: 1023,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
          arrows:false,
          customPaging: (i) => (
            <div
              style={{
                left: "10px",
                width: "10px",
                height: "10px",
                borderRadius: "20px",
                backgroundColor: "#c4c4c4"
              }}
            ></div>
          ),
        }
      }
    ]
  };
  return (
    <div className="content-carousel content-carousel-mobile">
      <div className="mb-md-4 slick-controls">
        {button === "left" ? (
          <button
            className="px-3 py-1 btn-carousel-left btn-carousel-active"
            onClick={create}
          >
            Create
          </button>
        ) : (
          <button className="px-3 py-1 btn-carousel-left " onClick={create}>
            Create
          </button>
        )}
        {button === "middle" ? (
          <button
            className="px-3 py-1 btn-carousel-middle btn-carousel-active"
            onClick={pause}
          >
            Pause
          </button>
        ) : (
          <button className="px-3 py-1 btn-carousel-middle" onClick={pause}>
            Pause
          </button>
        )}
        {button === "right" ? (
          <button
            className="px-3 py-1 btn-carousel-right btn-carousel-active"
            onClick={stop}
          >
            Stop
          </button>
        ) : (
          <button className="px-3 py-1 btn-carousel-right" onClick={stop}>
            Stop
          </button>
        )}
      </div>

      <Slider {...settings}>
        {slides.map(function (slide) {
          return (
            <div className="content-carousel-card" key={slide}>
              <div className="card-content-wrapper px-5 py-3">
                {" "}
                {/* Add this wrapper to handle the style of the item */}
                <div className="text-box px-3">
                  <h3 style={{ fontSize: "20px" }}>{data[slide - 1][0]}</h3>
                  <h3 style={{ fontSize: "20px" }} className="b-700">
                    {data[slide - 1][1]}
                  </h3>
                </div>
                <div className="img-box px-3">
                  <img
                    src={data[slide - 1][2]}
                    alt=""
                  />
                </div>
              </div>
            </div>
          );
        })}
      </Slider>
    
    </div>
  )
}

export default ContentCarousel
