import { useState } from "react";
import Slider from "react-slick";
import "./contentcarousel.scss"
import img1 from "../../../../../asset/img/wincarousel-phone1.png"
import img2 from "../../../../../asset/img/wincarousel-phone2.png"
import img3 from "../../../../../asset/img/win-phone1.png"
import img4 from "../../../../../asset/img/wincarousel-phone3.png"
import img5 from "../../../../../asset/img/wincarousel-phone4.png"


const WinCarousel = (props) => {
  const [slides, setSlides] = useState([1, 2]);
  const [data, setData] = useState([
    ["Step 1","On your Home tab, Check In as you click on the coin available that day.",img1],
    ["Step 2","Your Check In is Successful! Come back tomorrow for more reward points.",img2]
  ])
  
  const [button, setButton] = useState("left")

  const checkin = () => {
    setSlides([1, 2])
    setData([
      ["Step 1","On your Home tab, Check In as you click on the coin available that day.",img1],
      ["Step 2","Your Check In is Successful! Come back tomorrow for more reward points.",img2]
    ])
    setButton("left")
  }
  const mission = () => {
    setSlides([1, 2])
    setData([
      ["Step 1","On your Reward tab, Select Mission.",img3],
      ["Step 2","Select on Weekly, Biweekly, or Monthly to see the missions and its Progress. When the mission is accomplished, you can Collect Point.",img4]
    ])
    setButton("middle")
  }
  const redeem = () => {
    setSlides([1, 2])
    setData([
      ["Step 1","On your Reward tab, Select Redeem Point.",img3],
      ["Step 2","Claim the vouchers that you want.",img5]
    ])
    setButton("right")
  }
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    customPaging: (i) => (
      <div
        style={{
          left: "10px",
          width: "10px",
          height: "10px",
          borderRadius: "20px",
          backgroundColor: "#c4c4c4"
        }}
      ></div>
    ),responsive: [
      {
        breakpoint: 1023,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
          arrows:false,
          customPaging: (i) => (
            <div
              style={{
                left: "10px",
                width: "10px",
                height: "10px",
                borderRadius: "20px",
                backgroundColor: "#c4c4c4"
              }}
            ></div>
          ),
        }
      }
    ]
  };
  return (
    <div className="content-carousel content-carousel-mobile">
      <div className="mb-md-4 slick-controls">
        {button === "left" ? (
          <button
            className="px-3 py-1 btn-carousel-left btn-carousel-active"
            onClick={checkin}
          >
            Check In
          </button>
        ) : (
          <button className="px-3 py-1 btn-carousel-left " onClick={checkin}>
            Check In
          </button>
        )}
        {button === "middle" ? (
          <button
            className="px-3 py-1 btn-carousel-middle btn-carousel-active"
            onClick={mission}
          >
            Mission
          </button>
        ) : (
          <button className="px-3 py-1 btn-carousel-middle" onClick={mission}>
            Mission
          </button>
        )}
        {button === "right" ? (
          <button
            className="px-3 py-1 btn-carousel-right btn-carousel-active"
            onClick={redeem}
          >
            Redeem
          </button>
        ) : (
          <button className="px-3 py-1 btn-carousel-right" onClick={redeem}>
            Redeem
          </button>
        )}
      </div>

      <Slider {...settings}>
        {slides.map(function (slide) {
          return (
            <div className="content-carousel-card" key={slide}>
              <div className="card-content-wrapper px-5 py-3">
                {" "}
                {/* Add this wrapper to handle the style of the item */}
                <div className="text-box px-3">
                  <h3 style={{ fontSize: "20px" }}>{data[slide - 1][0]}</h3>
                  <h3 style={{ fontSize: "20px" }} className="b-700">
                    {data[slide - 1][1]}
                  </h3>
                </div>
                <div className="img-box px-3">
                  <img
                    src={data[slide - 1][2]}
                    alt=""
                  />
                </div>
              </div>
            </div>
          );
        })}
      </Slider>
    
    </div>
  )
}

export default WinCarousel
