import { Container } from "reactstrap"
import ContentCarousel2 from "./ContentCarousel2"

const JagaCarousel = () => {
    return (
        <div className="content-carousel">
            <Container className="mt-4 mb-4 py-3">
                <div className="row">
                    <div className="col-lg-1"></div>
                    <div className="col-lg-10">
                        <h2 className="mb-3 b-700 title">How To</h2>
                        <div className="carousel-card">
                            <ContentCarousel2/>
                        </div>
                    </div>
                    <div className="col-lg-1"></div>
                </div>
            </Container>
        </div>
    )
}

export default JagaCarousel