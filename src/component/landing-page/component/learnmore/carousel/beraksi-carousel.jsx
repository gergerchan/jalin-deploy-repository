import { Container } from "reactstrap"
import ContentCarousel3 from "./ContentCarousel3"

const BeraksiCarousel = () => {
    return (
        <div className="content-carousel content-carousel-blue">
            <Container className="mt-4 mb-4 py-3">
                <div className="row">
                    <div className="col-lg-1"></div>
                    <div className="col-lg-10">
                        <h2 className="mb-3 b-700 title">How To</h2>
                        <div className="carousel-card">
                            <ContentCarousel3/>
                        </div>
                    </div>
                    <div className="col-lg-1"></div>
                </div>
            </Container>
        </div>
    )
}

export default BeraksiCarousel