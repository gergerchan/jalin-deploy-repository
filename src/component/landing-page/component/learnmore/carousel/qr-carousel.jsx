import { Container } from "reactstrap"
import QrCarousel from "./QrCarousel"


const Carousel = () => {
    return (
        <div className="content-carousel content-carousel-blue">
            <Container className="mt-4 py-3">
                <div className="row">
                    <div className="col-lg-1"></div>
                    <div className="col-lg-10">
                        <h2 className=" mt-3 mb-3 b-700 title">How To</h2>
                        <div className="carousel-card">
                            <QrCarousel/>
                        </div>
                    </div>
                    <div className="col-lg-1"></div>
                </div>
            </Container>
        </div>
    )
}

export default Carousel