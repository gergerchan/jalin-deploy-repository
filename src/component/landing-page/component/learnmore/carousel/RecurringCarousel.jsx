import { useState } from "react";
import Slider from "react-slick";
import "./contentcarousel.scss"
import img1 from "../../../../../asset/img/recurring-phone1.png"
import img2 from "../../../../../asset/img/recurring-phone2.png"
import img3 from "../../../../../asset/img/recurring-phone3.png"
import img4 from "../../../../../asset/img/recurring-phone4.png"
import img5 from "../../../../../asset/img/recurring-img2.png"
import img6 from "../../../../../asset/img/recurring-phone5.png"
import img7 from "../../../../../asset/img/recurring-phone6.png"



const RecurringCarousel = (props) => {
  const [slides, setSlides] = useState([1, 2, 3, 4, 5, 6]);
  const [data, setData] = useState([
    ["Step 1","On your Home tab, Go to Recurring Transaction by clicking on See All.",img1],
    ["Step 2","Choose Plus (+) icon.",img2],
    ["Step 3","Choose Bank Transfer, Top-Up E-wallet, or Billing & Internet, depending on your needs.",img5],
    ["Step 4","Choose destination of registered bank account, phone number (top up e-wallet), billing payment account or Add New Account",img7],
    ["Step 5","Enter the amount you want to set for the transfer/payment, or choose Always Ask if you want to input manually everytime it is due. Then, Click on Next.",img6],
    ["Step 6","Choose the frequency, starting and ending date. Review your payment and Save your Recurring Transaction.",img4]
  ])
  
  const [button, setButton] = useState("left")

  const create = () => {
    setSlides([1, 2, 3, 4, 5, 6])
    setData([
      ["Step 1","On your Home tab, Go to Recurring Transaction by clicking on See All.",img1],
      ["Step 2","Choose Plus (+) icon.",img2],
      ["Step 3","Choose Bank Transfer, Top-Up E-wallet, or Billing & Internet, depending on your needs.",img5],
      ["Step 4","Choose destination of registered bank account, phone number (top up e-wallet), billing payment account or Add New Account",img6],
      ["Step 5","Enter the amount you want to set for the transfer/payment, or choose Always Ask if you want to input manually everytime it is due. Then, Click on Next.",img7],
      ["Step 6","Choose the frequency, starting and ending date. Review your payment and Save your Recurring Transaction.",img4]
    ])
    setButton("left")
  }
  const stop = () => {
    setSlides([1, 2, 3, 4])
    setData([
      ["Step 1","On your Home tab, Go to Recurring Transaction by clicking on See All.",img1],
      ["Step 2","Choose the payment that you want to delete.",img2],
      ["Step 3","Click on Edit Icon on the Right.",img3],
      ["Step 4","Click on Delete Icon on the Right.",img4],
    ])
    setButton("middle")
  }
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    customPaging: (i) => (
      <div
        style={{
          left: "10px",
          width: "10px",
          height: "10px",
          borderRadius: "20px",
          backgroundColor: "#c4c4c4"
        }}
      ></div>
    ),responsive: [
      {
        breakpoint: 1023,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
          arrows:false,
          customPaging: (i) => (
            <div
              style={{
                left: "10px",
                width: "10px",
                height: "10px",
                borderRadius: "20px",
                backgroundColor: "#c4c4c4"
              }}
            ></div>
          ),
        }
      }
    ]
  };
  return (
    <div className="content-carousel content-carousel-mobile">
      <div className="mb-md-4 slick-controls">
        {button === "left" ? (
          <button
            className="px-3 py-1 btn-carousel-left btn-carousel-active"
            onClick={create}
          >
            Create
          </button>
        ) : (
          <button className="px-3 py-1 btn-carousel-left " onClick={create}>
            Create
          </button>
        )}
        {button === "middle" ? (
          <button
            className="px-3 py-1 btn-carousel-middle btn-carousel-active"
            onClick={stop}
          >
            Stop
          </button>
        ) : (
          <button className="px-3 py-1 btn-carousel-right" onClick={stop}>
            Stop
          </button>
        )}
      </div>

      <Slider {...settings}>
        {slides.map(function (slide) {
          return (
            <div className="content-carousel-card" key={slide}>
              <div className="card-content-wrapper px-5 py-3">
                {" "}
                {/* Add this wrapper to handle the style of the item */}
                <div className="text-box px-3">
                  <h3 style={{ fontSize: "20px" }}>{data[slide - 1][0]}</h3>
                  <h3 style={{ fontSize: "20px" }} className="b-700">
                    {data[slide - 1][1]}
                  </h3>
                </div>
                <div className="img-box px-3">
                  <img
                    src={data[slide - 1][2]}
                    alt=""
                  />
                </div>
              </div>
            </div>
          );
        })}
      </Slider>
    
    </div>
  )
}

export default RecurringCarousel
