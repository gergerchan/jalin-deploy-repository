import Footer from "../footer/Footer"
import TopNavbar from "../header/TopNavbar"
import LearnRight from "./learn-component/learn-right"
import LearnLeft2 from "./learn-component/learn-left2"
import Contentjalin from "../content/Contentjalin"
import Carousel from "./carousel/mutation-carousel"
import img1 from "../../../../asset/img/mutation-img1.svg"
import img2 from "../../../../asset/img/mutation-phone1.png"
import img3 from "../../../../asset/img/mutation-phone2.png"
import candle from "../../../../asset/img/home-candle.svg"

const LearnMutation = ({isOpenModal, toggleModal, closeBtnModal}) => {
    return (
        <>
        <TopNavbar
                isOpenModal={isOpenModal} 
                toggleModal={toggleModal}
                closeBtnModal={closeBtnModal}
            />
        <LearnRight 
            titleTop="Mutation"
            title="Graphic Mutation"
            content="Viewing your bank account mutation has never been this easy. Get your summary and detail report on expenditure and income visualised."
            image={img1}
            titletype={true}
        />
        <LearnLeft2
            styling="content-bgcolor-lightblue"
            title="View your mutation with ease now!"
            content="View your income and expenditure summary in graphic format. You also can view the mutation in any periods flexibly."
            image={img2}
            image2={img3}
        />
        <Carousel/>
        <Contentjalin 
            styling="content-bgcolor-whiteblue"
            title="Keep the candle burning, so your neighbour might think you do Ngepet Ritual"
            content="Register a Jalin Account to get the latest solution for banking activities."
            button="Get Jalin"
            stylebtn="content-btn"
            image={candle}
            toggleModal={toggleModal}
        />
        <Footer/>
        </>
    )
}

export default LearnMutation