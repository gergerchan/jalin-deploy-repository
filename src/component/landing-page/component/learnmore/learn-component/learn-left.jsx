import { Container } from "reactstrap"

const LearnLeft = (props) => {
    return (
        <>
            <div className={`content py-5 mt-xs-0 py-xs-0 ${props.styling}`}>
                <Container className="py-2">
                    <div className="row">
                        <div className="col-lg-1"></div>
                        <div className="col-lg-3 d-flex justify-content-center">
                            <img src={props.image} className="content-image1 img-fluid" alt=""/>
                        </div>
                        <div className="col-lg-7 mt-lg-5 py-lg-5 mt-xs-0 py-xs-0 learn-text">
                            <h2 className="b-700 f-50 mb-4 learn-title">{props.title}</h2>
                            <p className="f-20 learn-content">{props.content}</p>
                            <p className="f-20 learn-content">{props.content2}</p>                             
                        </div>
                        <div className="col-lg-1"></div>
                        <div className="col-lg-3 d-flex justify-content-center">
                            <img src={props.image} className="content-image2 img-fluid" alt=""/>
                        </div>
                    </div>
                </Container>
            </div>
        </>
    )
}

export default LearnLeft