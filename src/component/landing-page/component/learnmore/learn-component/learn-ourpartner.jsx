import { Container } from "reactstrap"
import mcd from "../../../../../asset/img/qr-icon2.png"
import anne from "../../../../../asset/img/qr-icon3.png"
import sbux from "../../../../../asset/img/qr-icon4.png"
import ph from "../../../../../asset/img/qr-icon5.png"
import hnm from "../../../../../asset/img/qr-icon6.png"
import domino from "../../../../../asset/img/qr-icon7.png"
import polo from "../../../../../asset/img/qr-icon8.png"
import ace from "../../../../../asset/img/qr-icon9.png"
import zara from "../../../../../asset/img/qr-icon10.png"
import apple from "../../../../../asset/img/qr-icon11.png"
import adidas from "../../../../../asset/img/qr-icon12.png"
import indihom from "../../../../../asset/img/qr-icon13.png"
import br from "../../../../../asset/img/qr-icon14.png"
import bk from "../../../../../asset/img/qr-icon15.png"
import kfc from "../../../../../asset/img/qr-icon16.png"

const LearnPartners = (props) => {
    return (
        <>
            <div className={`content-learn-partner py-5 mt-xs-0 py-xs-0 ${props.styling}`}>
                <Container className="py-2">
                    <div className="row text-center">
                        <div className="col-12 mb-lg-2">
                            <h2 className="b-700 "><span className="bottom-line pb-2">Our Partners</span></h2>
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={mcd} alt="" className="icon-ourpartner"/>
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={anne} alt="" className="icon-ourpartner"/> 
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={sbux} alt="" className="icon-ourpartne"/>
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={ph} alt="" className="icon-ourpartner"/>
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={hnm} alt="" className="icon-ourpartner"/>
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={domino} alt="" className="icon-ourpartner" />
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={polo} alt="" className="icon-ourpartner" />
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={ace} alt="" className="icon-ourpartner" />
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={zara} alt="" className="icon-ourpartner" />
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={apple} alt="" className="icon-ourpartner" />    
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={adidas} alt="" className="icon-ourpartner" />
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={indihom} alt="" className="icon-ourpartner" />
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={br} alt="" className="icon-ourpartner" />
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={bk} alt="" className="icon-ourpartner" />
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={kfc} alt="" className="icon-ourpartner" /> 
                        </div>
                    </div>
                </Container>
            </div>
        </>
    )
}

export default LearnPartners




