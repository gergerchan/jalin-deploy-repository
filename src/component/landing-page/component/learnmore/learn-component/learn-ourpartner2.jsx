import { Container } from "reactstrap"
import icon1 from "../../../../../asset/img/recurring-icon1.png"
import icon2 from "../../../../../asset/img/recurring-icon2.png"
import icon3 from "../../../../../asset/img/recurring-icon3.png"
import icon4 from "../../../../../asset/img/recurring-icon4.png"
import icon5 from "../../../../../asset/img/recurring-icon5.png"
import icon6 from "../../../../../asset/img/recurring-icon6.png"
import icon7 from "../../../../../asset/img/recurring-icon7.png"
import icon8 from "../../../../../asset/img/recurring-icon8.png"
import icon9 from "../../../../../asset/img/recurring-icon9.png"
import icon10 from "../../../../../asset/img/recurring-icon10.png"
import icon11 from "../../../../../asset/img/recurring-icon11.png"
import icon12 from "../../../../../asset/img/recurring-icon12.png"
import icon13 from "../../../../../asset/img/recurring-icon13.png"
import icon14 from "../../../../../asset/img/recurring-icon14.png"
import icon15 from "../../../../../asset/img/recurring-icon15.png"
import icon16 from "../../../../../asset/img/recurring-icon16.png"
import icon17 from "../../../../../asset/img/recurring-icon17.png"
import icon18 from "../../../../../asset/img/recurring-icon18.png"
import icon19 from "../../../../../asset/img/recurring-icon19.png"
import icon20 from "../../../../../asset/img/recurring-icon20.png"
import icon21 from "../../../../../asset/img/recurring-icon21.png"
import icon22 from "../../../../../asset/img/recurring-icon22.png"
import icon23 from "../../../../../asset/img/recurring-icon23.png"
import icon24 from "../../../../../asset/img/recurring-icon24.png"
import icon25 from "../../../../../asset/img/recurring-icon25.png"


const LearnPartners2 = (props) => {
    return (
        <>
            <div className={`content-learn-partner py-5 mt-xs-0 py-xs-0 ${props.styling}`}>
                <Container className="py-2">
                    <div className="row text-center">
                        <div className="col-12 mb-lg-2">
                            <h2 className="b-700 "><span className="bottom-line pb-2">Our Partners</span></h2>
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon1} alt="" className="icon-ourpartner"/>
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon2} alt="" className="icon-ourpartner"/> 
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon3} alt="" className="icon-ourpartne"/>
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon4} alt="" className="icon-ourpartner"/>
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon5} alt="" className="icon-ourpartner"/>
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon6} alt="" className="icon-ourpartner" />
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon7} alt="" className="icon-ourpartner" />
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon8} alt="" className="icon-ourpartner" />
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon9} alt="" className="icon-ourpartner" />
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon10} alt="" className="icon-ourpartner" />    
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon11} alt="" className="icon-ourpartner" />
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon12} alt="" className="icon-ourpartner" />
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon13} alt="" className="icon-ourpartner" />
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon14} alt="" className="icon-ourpartner" />
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon15} alt="" className="icon-ourpartner" /> 
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon16} alt="" className="icon-ourpartner" /> 
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon17} alt="" className="icon-ourpartner" /> 
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon18} alt="" className="icon-ourpartner" /> 
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon19} alt="" className="icon-ourpartner" /> 
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon20} alt="" className="icon-ourpartner" /> 
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon21} alt="" className="icon-ourpartner" /> 
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon22} alt="" className="icon-ourpartner" /> 
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon23} alt="" className="icon-ourpartner" /> 
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon24} alt="" className="icon-ourpartner" /> 
                        </div>
                        <div className="col px-lg-5 px-1 px-md-2 py-3">
                            <img src={icon25} alt="" className="icon-ourpartner" /> 
                        </div>
                    </div>
                </Container>
            </div>
        </>
    )
}

export default LearnPartners2




