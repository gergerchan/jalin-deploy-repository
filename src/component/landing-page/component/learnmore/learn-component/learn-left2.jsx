import { Container } from "reactstrap"
import './learnmore.scss'

const LearnLeft2 = (props) => {
    return (
        <>
            <div className={`content py-5 ${props.styling}`}>
                <Container className="py-2">
                    <div className="row">
                        <div className="col-lg-1"></div>
                        <div className="col-lg-5">
                            <img src={props.image} className="content-image1 img-fluid" alt=""/>
                            <img src={props.image2} className="content-image1 img-fluid" alt="" />
                        </div>
                        <div className="col-lg-6 mt-lg-6 py-lg-5 mt-xs-0 py-xs-0 learn-text">
                            <h2 className="b-700 f-50 mb-4">{props.title}</h2>
                            <p className="f-20 mb-5">{props.content}</p>                            
                        </div>
                        <div className="col-lg-3 d-flex justify-content-center">
                            <img src={props.image} className="content-image2 " alt="learnmore-jalin"/>
                        </div>
                        <div className="col-lg-3 d-flex justify-content-center">
                            <img src={props.image2} className="content-image2 "alt="learnmore-jalin"/>
                        </div>
                    </div>
                </Container>
            </div>
        </>
    )
}

export default LearnLeft2