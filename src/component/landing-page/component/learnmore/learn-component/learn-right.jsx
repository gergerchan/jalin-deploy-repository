import { Container } from "reactstrap"
import './learnmore.scss'

const LearnRight = (props) => {
    return (
        <>
            <div className={`content py-5 my-lg-4 ${props.styling}`}>
                <Container className="py-2">
                    <div className="row">
                        <div className="col-lg-1"></div>
                        <div className="col-lg-7 mt-lg-3 py-lg-4 mt-xs-0 py-xs-0 learn-text">
                            <h3 className="b-700 f-20 learn-title">{props.titleTop}</h3>
                            <h2 className="b-700 f-50 mb-4 learn-title">{props.title}</h2>
                            {props.titletype ? 
                                <>
                                    <img src={props.image} className="content-image img-fluid learn-mobile mx-auto mb-3" alt=""/>
                                    <p className="f-20 learn-content learn-web">{props.content}</p> 
                                </> 
                            : 
                                <p className="f-20 learn-content">{props.content}</p> 
                            }
                                                      
                        </div>
                        <div className="col-lg-1"></div>
                        <div className="col-lg-3 d-flex justify-content-center">
                        {props.titletype ? 
                            <>
                                <img src={props.image} className="content-image img-fluid learn-web" alt=""/>
                                <p className="f-20 learn-content learn-mobile">{props.content}</p>
                            </>
                        : 
                            <img src={props.image} className="content-image img-fluid" alt=""/>
                        }

                        </div>
                    </div>
                </Container>
            </div>
        </>
    )
}

export default LearnRight