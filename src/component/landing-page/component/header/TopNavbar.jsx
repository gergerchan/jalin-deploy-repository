import Logo from "../../../../asset/img/header_logo.svg"
import { Link, NavLink } from 'react-router-dom';
import { Container } from "reactstrap";
import QrcodeModals from '../qrcode-get-jalin/QrcodeModals';
import "./TopNavbar.scss"

const TopNavbar = ({ isOpenModal, toggleModal, closeBtnModal }) => {
    return (
        <div className="nav-border-bottom">
            <Container className="py-3">
                <div className="row d-flex align-items-center ">
                    <div className="col nav-image-wrapper">
                        <NavLink to="/"><img src={Logo} alt="" className="logo_jalin_top" /></NavLink>
                    </div>
                    <div className="col-auto flex-grow align-items-center px-0">
                        <NavLink to="/feature" className="b-700 px-lg-5 px-md-3 px-2 color-black" activeClassName="topnavbar-active">Features</NavLink>
                        <NavLink to="/promo" className="b-700 px-lg-5 px-md-3 px-2 color-black" activeClassName="topnavbar-active"> Promo</NavLink>
                        <Link onClick={toggleModal} className="b-700 px-lg-5 px-md-3 px-2"><span className="px-lg-4 px-3 py-2 nav-btn">Get Jalin</span></Link>
                    </div>
                </div>
                <QrcodeModals
                    isOpen={isOpenModal}
                    toggle={toggleModal}
                    closeBtnModal={closeBtnModal}
                />
            </Container>
        </div>
    )
}

export default TopNavbar
