import { Container } from "reactstrap"


const Contentfeature = (props) => {
    return (
        <>
            <div className={`content py-5 ${props.styling}`}>
                <Container className="py-2">
                    <div className="row">
                        <div className="col-lg-1"></div>
                        <div className="col-lg-7 mt-lg-5 py-lg-5 mt-xs-0 py-xs-0 feature-text">
                            <h1 className="b-700 f-50 feature-title">{props.title}</h1>
                            <p className="f-18 mb-5 feature-content">{props.content}</p>                            
                        </div>
                        <div className="col-lg-1"></div>
                        <div className="col-lg-3 p-0 d-flex justify-content-center">
                            <img src={props.image} className="feature-image-content" alt=""/>
                        </div>
                    </div>
                </Container>
            </div>
        </>
    )
}

export default Contentfeature