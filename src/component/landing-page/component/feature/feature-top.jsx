import { Container } from "reactstrap"
import "./feature.scss"

const ContentFeatureTop = (props) => {
    return (
        <>
        <div className="content pt-lg-5 pt-3 mb-3">
            <Container className="py-2">
                <div className="row">
                    <div className="col-lg-1"></div>
                    <div className="col-lg-7 mt-2">
                        <h1 className="b-700 f-50 feature-title-text">{props.title}</h1>
                        <p className="f-20 mb-3 feature-text-align">{props.content}</p>

                         {/* Card */}
                        <div className="row">
                            <div className="col-lg-6 my-3">
                                <div className={`card-content card-content-bgcolor-lightblue py-3 px-4` }>
                                    <div className="row">
                                        <div className="col-8 pe-0 ">
                                            <h3 className="b-700 mb-lg-3 feature-text-align">{props.cardTitle1}</h3>
                                            <p className="feature-text-align">{props.cardContent1}</p>
                                        </div>
                                        <div className="col-4 p-0 d-flex justify-content-center">
                                            <img src={props.cardImg1} className="topcard-image img-fluid" alt="jalin"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6 my-3">
                                <div className={`card-content card-content-bgcolor-lightblue py-3 px-4` }>
                                    <div className="row">
                                        <div className="col-8 pe-0">
                                            <h3 className="b-700 mb-lg-3 feature-text-align">{props.cardTitle2}</h3>
                                            <p className="feature-text-align">{props.cardContent2}</p>
                                        </div>
                                        <div className="col-4 p-0 d-flex justify-content-center">
                                            <img src={props.cardImg2} className="topcard-image img-fluid" alt="jalin"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>   
                    <div className="col-lg-1"></div>
                    <div className="col-lg-3 p-0 d-flex justify-content-center">
                        <img src={props.image} className="feature-image-top" alt="jalin"/>
                    </div>
                </div>
                
            </Container>
        </div>
        </>
    )
}

export default ContentFeatureTop