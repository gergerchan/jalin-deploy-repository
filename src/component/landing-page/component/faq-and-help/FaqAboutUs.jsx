import "./faq.scss";

const FaqAboutUs = () => {
    return (
        <div className='base container'>
            <div className='faq'>
                <div className="faq-header f-30 b-700 py-1 py-lg-3 py-md-2 py-sm-1 text-center">About Us</div>
                <div class="accordion accordion-flush" id="accordionFlushExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingOne">
                            <button class="accordion-button collapsed b-700 px-0 pt-4" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                What is Jalin?
                            </button>
                        </h2>
                        <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body f-20 px-0">
                                Bank Jalin is a life finance solutions provider supervised by Bank Indonesia and OJK.
                                We help you save & send money, top up e-Wallet, pay bills, request money from your friends, anytime and anywhere you like.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingTwo">
                            <button class="accordion-button collapsed b-700 px-0 pt-4" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                Is Jalin Protected by OJK/LPS?
                            </button>
                        </h2>
                        <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body f-20 px-0">
                                PT Bank Jalin Tbk is registered and supervised by Financial Services Authority (OJK) and
                                guaranteed by Indonesia Deposit Insurance Corporation (LPS).
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree">
                            <button class="accordion-button collapsed b-700 px-0 pt-4" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                What is the benefit of using Bank Jalin?
                            </button>
                        </h2>
                        <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body f-20 px-0">
                                Bank Jalin simplifies your life’s financial needs using state-of-the-art technology and guaranteed security.<br /><br />
                                We help you:
                                <ul>
                                    <li>Allocate your money to saving that you can personalize to better manage your finances and gain interests.</li>
                                    <li>Enjoy benefits by collecting points from Daily Check In and Missions.</li>
                                    <li>Schedule and automate money transfers and savings so you don't forget paying bills or transfer money to your closest friends & relatives.</li>
                                    <li>Viewing your bank account mutation easily by getting your summary and detail report on expenditure and income visualised.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingFour">
                            <button class="accordion-button collapsed b-700 px-0 pt-4" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                                What are the main requirements to be Jalin Customer?
                            </button>
                        </h2>
                        <div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body f-20 px-0">
                                You can apply for Jalin account if you are of <strong>Indonesian Nationality</strong> which
                                possesses an <strong>ID Card (e-KTP)</strong> and uses a smartphone.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingFive">
                            <button class="accordion-button collapsed b-700 px-0 pt-4" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                                What are the documents required to open Jalin Account?
                            </button>
                        </h2>
                        <div id="flush-collapseFive" class="accordion-collapse collapse" aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body f-20 px-0">
                                To complete registration, you have to prepare your original ID Card and NPWP if you have one. For the activation process by video call,
                                all you have to do is prepare your e-KTP, and follow the instruction from our crew. <br /><br />
                                <strong>What if I didn't have/yet to receive an ID Card (e-KTP)</strong><br /><br />
                                If you didn't have an ID Card, sadly you can't apply to Jalin.<br /><br />
                                <strong>Tip:</strong> You can use an old version of KTP, also e-KTP, as long as it's still in the validity period.<br /><br />
                                <strong>What if I didn't have/yet to receive NPWP</strong><br /><br />
                                You can still apply for Jalin without showing NPWP by choosing "I don't have NPWP" while registering. Jalin will still ask for your NPWP when you've done your activation process.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FaqAboutUs
