import "./faq.scss";

const FaqReward = () => {
    return (
        <div className='base container'>
            <div className='faq'>
                <div className="faq-header f-30 b-700 py-1 py-lg-3 py-md-2 py-sm-1 text-center">Reward</div>
                <div class="accordion accordion-flush" id="accordionFlushExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingOne">
                            <button class="accordion-button collapsed b-700 px-0 pt-4" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                What are Daily Missions and how to complete them?
                            </button>
                        </h2>
                        <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body f-20 px-0">
                                They are bite-sized challenges that reward you for using Jalin. Each challenge is personalized based on your saving and spending patterns. 
                                All you have to do is follow the steps to enjoy your Reward! Any terms & conditions will be 
                                mentioned in the card with links to our website to find out more. We promise there’ll be no further conditions, no hidden fine print.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingTwo">
                            <button class="accordion-button collapsed b-700 px-0 pt-4" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                I have completed my mission, when will I receive my reward?
                            </button>
                        </h2>
                        <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body f-20 px-0">
                                Each mission is different, so do refer to your mission card to find out when your reward will be sent by. <br/><br/>
                                If your reward is a cashback treat, you’ll receive a notification on your dashboard when it’s been credited, so do login to check the details.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree">
                            <button class="accordion-button collapsed b-700 px-0 pt-4" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                Why do I receive different missions from others?
                            </button>
                        </h2>
                        <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body f-20 px-0">
                                Your daily missions are personalized based on your saving and spending patterns with us. <br/><br/>
                                Be sure to keep your push notifications from Jalin on, so you’re always in the know! Your next mission might just be around the corner. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FaqReward
