import "./help.scss";
import helpCenter from '../../../../asset/img/call-center.svg';
import { useState } from "react"
import failedIcon from "../../../../asset/img/failed-check.svg";
import successIcon from "../../../../asset/img/success-check.svg";

const Help = () => {
    const [ name, setName ] = useState('');
    const [ phone, setPhone ] = useState('');
    const [ email, setEmail ] = useState('');
    const [ subject, setSubject ] = useState('');
    const [ message, setMessage ] = useState('');
    const [ successMessage, setSuccessMessage ] = useState(false);
    const [ failedMessage, setFailedMessage ] = useState(false);

    const handleName = (e) => {
        setName(e.target.value)
    }
    
    const handlePhone = (e) => {
        setPhone(e.target.value)
    }
    
    const handleEmail = (e) => {
        setEmail(e.target.value)
    }
    
    const handleSubject = (e) => {
        setSubject(e.target.value)
    }
    
    const handleMessage = (e) => {
        setMessage(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault()

        if ( name !== '' && phone !== '' && email !== '' && subject !== '' && message !== '') {
            setSuccessMessage(true)
            setFailedMessage(false)

            setTimeout(() => {
                setName('')
                setPhone('')
                setEmail('')
                setSubject('')
                setMessage('')
                setSuccessMessage(false) 
            }, 3000);
        } 

        if ( name === '' || phone === '' || email === '' || subject === '' || message === '' ) {
            setFailedMessage(true)
        }
    }

    // const handleChange = (e) => {
    //     const { name, value } = e.target
    //     setValues({
    //         ...values,
    //         [name]: value
    //     })
    // }

    return (
        <div className='base container py-lg-5 py-md-3'>
            <div className="help-title">
                <h3 className="help-title-header f-30 b-700">Help Center</h3>
                <p className="help-title-desc b-600 mb-0">Our 24/7 Jalin will never be broken. Do you have problems with Jalin and need help? 
                    Please contact us at the following services:
                </p>
            </div>
            <form onSubmit={handleSubmit}> 
                <div className='contact-box'>
                    <div className='contact-title-box text-center'>
                        <h3 className="contact-title-box-header f-30 b-700">Contact Us</h3>
                        <p className='contact-title-box-desc f-20 b-600'>We’re happy to answer any questions you may have, just send us a message via the form below!</p>
                    </div>
                    <div className='contact-form-box'>
                        <div className="row mb-lg-3">
                            <div className="col-lg-6 col-md-12 pe-lg-5 mb-2 mb-sm-2 mb-lg-0 mb-md-0">
                                <label class="form-label f-18 b-600">Your Name</label>
                                <input 
                                    type="text" 
                                    className={`form-control ${!name && failedMessage && 'invalid'}`}
                                    placeholder="John Doe"
                                    name='name' 
                                    onChange={handleName}
                                    value={name}
                                />
                                { !name && failedMessage && <p className='text-danger'>Please fill in your name</p>}
                            </div>
                            <div className="col-lg-6 col-md-12 ps-lg-5 mb-2 mb-sm-2 mb-lg-0 mb-md-0">
                                <label class="form-label f-18 b-600">Phone Number</label>
                                <input 
                                     type="tel" pattern="[+]{6}{2}[0-9]{11,15}" 
                                    className={`form-control ${!phone && failedMessage && 'invalid'}`} 
                                    placeholder="+62808080808" 
                                    name='phone'
                                    onChange={handlePhone}
                                    value={phone}
                                />
                                { !phone && failedMessage && <p className='text-danger'>Please fill in your phone number</p>}
                            </div>
                        </div>
                        <div className="row mb-lg-3">
                            <div className="col-lg-6 col-md-12 pe-lg-5 mb-2 mb-sm-2 mb-lg-0 mb-md-0">
                                <label class="form-label f-18 b-600">Email Address</label>
                                <input 
                                    type="email" 
                                    className={`form-control ${!email && failedMessage && 'invalid'}`}
                                    placeholder="johndoe@gmail.com" 
                                    name='email'
                                    onChange={handleEmail}
                                    value={email}
                                />
                                { !email && failedMessage && <p className='text-danger'>Please fill in your email address</p>}
                            </div>
                            <div className="col-lg-6 col-md-12 ps-lg-5 mb-2 mb-sm-2 mb-lg-0 mb-md-0">
                                <label class="form-label f-18 b-600">Subject</label>
                                <input 
                                    type="text" 
                                    className={`form-control ${!subject && failedMessage && 'invalid'}`}
                                    placeholder="Write the subject" 
                                    name='subject'
                                    onChange={handleSubject}
                                    value={subject}
                                />
                                { !subject && failedMessage && <p className='text-danger'>Please fill in subject</p>}
                            </div>
                        </div>
                        <div className="row mb-4 mx-0">
                            <label class="form-label f-18 b-600 px-0">Message</label>
                            <textarea 
                                className={`form-control ${!message && failedMessage && 'invalid'}`}
                                style={{height:"120px"}} 
                                placeholder='Write your message' 
                                rows="5"
                                name='message'
                                onChange={handleMessage}
                                value={message}
                            />
                            { !message && failedMessage && <p className='text-danger px-0'>Please fill in your message</p>}
                        </div>
                        <div className="row message-btn-box mx-0">
                            <div className="col-lg-6 col-12 
                                d-md-flex justify-content-md-center 
                                d-lg-flex justify-content-lg-start 
                                d-flex align-items-center 
                                px-0 px-0 pb-md-4 pb-sm-4 pb-4 pb-lg-0 
                                message-btn-box-left"
                            >
                                { successMessage &&
                                    <>
                                        <div className="row">
                                            <div className="col-lg-1 col-md-1 col-2"><img src={successIcon} alt="success icon" /></div>
                                            <div className="col-lg-11 col-md-11 col-10 f-18 px-lg-3 px-md-3 px-0">
                                                Thank you for your message. It has been sent.
                                            </div>
                                        </div>  
                                    </>
                                }

                                { failedMessage &&
                                    <>
                                        <div className="row">
                                            <div className="col-lg-1 col-md-1 col-2"><img src={failedIcon} alt="failed icon" /></div>
                                            <div className="col-lg-11 col-md-11 col-10 f-18 px-lg-3 px-md-3 px-0">
                                                There was an error sending your message. Please try again later. 
                                            </div>
                                        </div>  
                                    </>
                                }
                            </div>
                            <div className='col-lg-6 col-12 px-0 d-lg-flex justify-content-lg-end d-md-flex justify-content-md-center d-sm-flex justify-content-sm-center d-flex justify-content-center message-btn-box-right'>
                                <button className='message-btn'>
                                    Send Message
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div className='help-center-box py-5'>
                <div className='help-center-title row'>
                    <div className="help-center-icon col-lg-5 col-md-4 col-sm-3 col-3 text-end"><img src={helpCenter} alt="help center icon" /></div>
                    <div className="help-center-desc col-lg-7 col-md-8 col-sm-9 col-9 f-15 d-flex justify-content-start align-items-center" style={{lineHeight:'18px'}}>
                        Contact us for issues or send us feedback, We love to hear from you!
                    </div>
                </div>
                <div className="working-hours pb-4">
                    <h6 className='b-600 mb-2 text-center'>Working Hours</h6>
                    <div className='row py-1'>
                        <div className="col-6 f-13 text-end">Weekday</div>
                        <div className="col-6 f-13">: 8 AM to 5 PM</div>
                    </div>
                    <div className='row py-1'>
                        <div className="col-6 f-13 text-end">Weekend</div>
                        <div className="col-6 f-13">: 9 AM to 3 PM</div>
                    </div>
                </div>
                <div className="call-center">
                    <h6 className='b-600 mb-2 text-center'>Call Center</h6>
                    <div className='row py-1'>
                        <div className="col-6 f-13 text-end">Hotline Call</div>
                        <div className="col-6 f-13">: 1500 777</div>
                    </div>
                    <div className='row py-1'>
                        <div className="col-6 f-13 text-end">Phone Call</div>
                        <div className="col-6 f-13">: (021) 3000 8888</div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Help
