import "./faq.scss";

const FaqPayment = () => {
    return (
        <div className='base container'>
            <div className='faq'>
                <div className="faq-header f-30 b-700 py-1 py-lg-3 py-md-2 py-sm-1 text-center">Payment</div>
                <div class="accordion accordion-flush" id="accordionFlushExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingOne">
                            <button class="accordion-button collapsed b-700 px-0 pt-4" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                Send Money
                            </button>
                        </h2>
                        <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body f-20 px-0">
                                Between Jalin <br/>
                                <ol>
                                    <li>Go to Bank Transfer on the Home page.</li>
                                    <li>Search from an existing contact, or choose New Contact.</li>
                                    <li>Input the amount and tap Next.</li>
                                    <li>Review the transfer summary, then choose to Send Now.</li>
                                </ol>
                                <br/>

                                Tip: You can make recurring transaction after this step <br/><br/> 
                                
                                To other banks <br/>
                                <ol>
                                    <li>Go to  Bank Transfer on the Home page.</li>
                                    <li>Search from an existing contact, or choose New Contact.</li>
                                    <li>Choose Bank.</li>
                                    <li>Choose Other Bank.</li>
                                    <li>Input account number, click Check and tap Next.</li>
                                    <li>Input the amount and tap Next. </li>
                                    <li>Review the transfer summary, then choose to Send Now.</li>
                                </ol>
                                <br/>
                                Tip: You can make recurring transaction after this step
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingTwo">
                            <button class="accordion-button collapsed b-700 px-0 pt-4" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                About Pay and Send
                            </button>
                        </h2>
                        <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body f-20 px-0">
                                To make new transactions, you need to add the destination account number, e-Wallet number, 
                                or bill number that hasn't been registered in Jalin's contact by selecting the New Contact button. <br/><br/>
                                
                                What is the maximum transfer limit to Other Bank?
                                <ul>
                                    <li>Rp50.000.000/transaction</li>
                                    <li>Rp100.000.000/daily</li>
                                </ul>
                                
                                What is the maximum limit transaction in e-Wallet?
                                <ul>
                                    <li>Rp50.000.000/transaction</li>
                                    <li>Rp100.000.000/daily </li>
                                </ul> 
                                
                                What bills can be paid through Jalin?
                                <ul>
                                    <li>Mobile data purchase</li>
                                    <li>Mobile top up</li>
                                    <li>Telkom (Indihome)</li>
                                    <li>Biznet</li>
                                </ul>
                                
                                What is the maximum limit transaction to pay bills?
                                <ul>
                                    <li>Rp25.000.000/transaction</li>
                                    <li>Rp100.000.000/daily</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree">
                            <button class="accordion-button collapsed b-700 px-0 pt-4" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                What is QR Payment?
                            </button>
                        </h2>
                        <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body f-20 px-0">
                                QR Payment Transaction made by scanning a merchant’s Quick Response Code Indonesian Standard (QRIS) QR Code via Jalin App. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FaqPayment
