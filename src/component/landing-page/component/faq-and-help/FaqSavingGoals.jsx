import "./faq.scss";

const FaqSavingGoals = () => {
    return (
        <div className='base container'>
            <div className='faq'>
                <div className="faq-header f-30 b-700 py-1 py-lg-3 py-md-2 py-sm-1 text-center">Saving Goals</div>
                <div class="accordion accordion-flush" id="accordionFlushExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingOne">
                            <button class="accordion-button collapsed b-700 px-0 pt-4" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                About Saving
                            </button>
                        </h2>
                        <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body f-20 px-0">
                                A Saving is where you put your money in Jalin. It allows you to separate your money into multiple Savings based on your needs. 
                                Personalised it by giving your savings any name you’d like, upload a photo or use emojis, and use your favorite color as the background.<br/><br/>
                                You can set up your Saving for different purposes. To ease your daily transactions, choose Jaga Aja where you can attach a Visa debit 
                                card to it (coming soon) and use it for e-commerce shopping or withdraw in any ATMs around the globe. To motivate you on your life goals, 
                                choose Ikut Beraksi and you can set up a target to see your saving progress and visualised how close you are to your goal; on top of that 
                                you can also activate autosave function that will top-up the savings for you.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingTwo">
                            <button class="accordion-button collapsed b-700 px-0 pt-4" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                Personalizing Saving
                            </button>
                        </h2>
                        <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body f-20 px-0">
                                Choose between emoji or image <br/>
                                You can use your phone emoji as your Saving’s display picture or upload your favorite photo from your phone storage if you prefer to do so.<br/><br/>
                                Change background color <br/>
                                The different colors you can choose for every Saving will help freshen and personalize your money to your liking.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree">
                            <button class="accordion-button collapsed b-700 px-0 pt-4" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                Make a Target Saving
                            </button>
                        </h2>
                        <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body f-20 px-0">
                                Setting up a target will let you decide the amount of money to achieve. Your current Saving balance is included in the calculation, 
                                so make sure to have a bigger target than your current balance. <br/><br/>
                                You can also set up a deadline (optional) to achieve your target.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FaqSavingGoals
