import "../card-promo/cardPromo.scss";
import { NavLink, Route } from 'react-router-dom';
import jalinIcon from "../../../../asset/img/logo_jalin 1.svg";
import piggyIcon from "../../../../asset/img/piggy-bank.svg";
import rewardIcon from "../../../../asset/img/reward.svg";
import cashlessIcon from "../../../../asset/img/cashless-payment.svg";
import FaqAboutUs from "./FaqAboutUs";
import FaqSavingGoals from "./FaqSavingGoals";
import FaqReward from "./FaqReward";
import FaqPayment from "./FaqPayment";


const Faq = () => {
    return (
        <div className='base pt-1 pt-lg-5 pt-md-3 pt-sm-2'>
            <div className='card-promo container'>
                <div className="row card-promo-title">
                    <h3 className="card-promo-title-header f-30 b-700">Frequently Asked Questions</h3>
                    <p className="card-promo-title-subheader b-600 mb-0">A set of common questions asked. Find all the information you need.</p>
                </div>
                <div className="row card-promo-nav">
                    <div className="card-promo-nav-icon col-6 col-lg-3 p-0 d-flex justify-content-center">
                        <NavLink exact to='/faq' className="card-promo-nav-icon-item text-center" activeClassName='card-promo-nav-active'>
                            <img src={jalinIcon} alt="jalin icon" />
                            <p className="card-promo-nav-icon-item-title b-600 f-18 mt-3 mb-0">About Us</p>
                        </NavLink>
                    </div>
                    <div className="card-promo-nav-icon col-6 col-lg-3 p-0 d-flex justify-content-center">
                        <NavLink to='/faq/saving-goals' className="card-promo-nav-icon-item text-center" activeClassName='card-promo-nav-active'>
                            <img src={piggyIcon} alt="piggy bank icon" />
                            <p className="card-promo-nav-icon-item-title b-600 f-18 mt-3 mb-0">Saving Goals</p>
                        </NavLink>
                    </div>
                    <div className="card-promo-nav-icon col-6 col-lg-3 p-0 d-flex justify-content-center">
                        <NavLink to='/faq/reward'className="card-promo-nav-icon-item text-center" activeClassName='card-promo-nav-active'>
                            <img src={rewardIcon} alt="reward icon" />
                            <p className="card-promo-nav-icon-item-title b-600 f-18 mt-3 mb-0">Reward</p>
                        </NavLink>
                    </div>
                    <div className="card-promo-nav-icon col-6 col-lg-3 p-0 d-flex justify-content-center">
                        <NavLink to='/faq/payment'className="card-promo-nav-icon-item text-center" activeClassName='card-promo-nav-active'>
                            <img src={cashlessIcon} alt="cashless payment icon" />
                            <p className="card-promo-nav-icon-item-title b-600 f-18 mt-3 mb-0">Payment</p>
                        </NavLink>
                    </div>
                </div>
            </div>

            <hr className='card-promo-bottom-line mt-0'/>

            <Route exact path='/faq'>
                <FaqAboutUs />
            </Route>
            <Route exact path='/faq/saving-goals'>
                <FaqSavingGoals />
            </Route>
            <Route exact path='/faq/reward'>
                <FaqReward />
            </Route>
            <Route exact path='/faq/payment'>
                <FaqPayment />
            </Route>
        </div>
    )
}

export default Faq
